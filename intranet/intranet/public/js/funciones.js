/**
 * Created by fernando on 10/03/17.
 */


function nombreDia(){
    var buscado = $("#desde").val();
    var url = ip+'/fechas/fechadia/'+buscado;

    $.ajax({
        type:"GET",
        url: url,
        success: function(datos){
            var reg = datos.split("#");
            $("#nroDia").val(reg[0]);
            $("#nombreDia").html(reg[1]);
        }
    });
}

function encerarHExtra(){
    
    var blanco = "";
    var cero =0;

    $("#ingresoDetalle").html(blanco);
    $("#salidaReceso").html(blanco);
    $("#ingresoReceso").html(blanco);
    $("#SalidaDetalle").html(blanco);
    $("#hextra25").val(blanco);
    $("#hextra50").val(blanco);
    $("#hextra100").val(blanco);
    $("#frmIngreso").get(0).reset();
    
}

function accesoDia(cedula, pagadomes,pagadoano){
    var fecha = $("#desde").val();
    var url = ip+'/accesos/accesofecha4/'+cedula+'/'+fecha+'/'+pagadomes+'/'+pagadoano+'/0';
    $.ajax({
        type:"GET",
        url: url,
        success: function(datos){
            
            registros = datos.split("#");

            if(registros.length<2){
                alert("No existe datos para "+fecha);
     //           encerarHExtra();
                return 0;
            }

            var ingreso = registros[0];         //registros de ingreso
            ing = ingreso.split("$");
            
            if(ing.length>1){
                $("#ingreso").val(ing[1].trim());
                var ingresoDetalle = "";
                var inc = 0;
                ing.forEach(function(reg){
                    if(inc > 1)
                        ingresoDetalle = ingresoDetalle + reg+" ";
                    inc ++;
                });               
                $("#ingresoDetalle").html(ingresoDetalle);
            }
            receso = registros[1];          //regsitros de receso
            rec = receso.split("$");
            if(rec.length>1) {
                $("#salReceso").val(rec[1].trim());
                var ingReceso = rec[2];
                var recesoDetalle = "";
                if (rec.length > 2) {
                    var inc = 0;
                    rec.forEach(function (reg) {
                        inc++;
                        if (inc > 2 && inc < rec.length)
                            recesoDetalle = recesoDetalle + reg + " ";
                        if (inc == rec.length)
                            ingReceso = reg;
                    });
                }
            }
            $("#ingReceso").val(ingReceso);
            $("#salidaReceso").html(recesoDetalle);
            
            salida = registros[2];          //regsitros de salida
            sal = salida.split("$");
            if(sal.length>1) {
                $("#salida").val(sal[1].trim());
                if (sal.length > 1) {
                    var salidaDetalle = "";
                    var inc = 0;
                    sal.forEach(function (reg) {
                        if (inc > 1)
                            salidaDetalle = salidaDetalle + reg + " ";
                        inc++;
                    });
                    $("#salidaDetalle").html(salidaDetalle);
                }
            }
            var hextra25 = registros[3];    //total de horas extra
            hextra25 = hextra25.trim();
            $("#hextra25").val(hextra25);
            var hextra50 = registros[4];    //total de horas extra
            hextra50 = hextra50.trim();
            $("#hextra50").val(hextra50);
            var hextra100 = registros[5];
            hextra100 = hextra100.trim();
            $("#hextra100").val(hextra100);
            
            var sumaHorasExtra = registros[6];
            sumaHorasExtra = sumaHorasExtra.split("$");
            $("#suma25").html(sumaHorasExtra[0]);
            $("#suma50").html(sumaHorasExtra[1]);
            $("#suma100").html(sumaHorasExtra[2]);
            $("#totalhe25").val(sumaHorasExtra[0].trim());
            $("#totalhe50").val(sumaHorasExtra[1].trim());
            $("#totalhe100").val(sumaHorasExtra[2].trim());
            
            var totalHorasExtra = registros[7];
            totalHorasExtra = totalHorasExtra.split("$");
            $("#limite25").html(totalHorasExtra[0]);
            $("#limite50").html(totalHorasExtra[1]);
            $("#limite100").html(totalHorasExtra[2]);
            $("#limitehe25").val(totalHorasExtra[0].trim());
            $("#limitehe50").val(totalHorasExtra[1].trim());
            $("#limitehe100").val(totalHorasExtra[2].trim());

            var anterior = registros[8].trim();
            var ant = anterior.split("$");
            if(ant.length>1){
                $("#idregistro").val(ant[0].trim());
                $mensaje = ant[1].trim();
                if($mensaje.length>1)
                    alert(ant[1]);
            }
            
            var alerta = registros[9].trim();            
            if(alerta.length>3){
                alert(alerta);
//                encerarHExtra();
//                return 0;
            }

        }
    });
}

function enviar(){
    var hextra25 = $("#hextra25").val();
    var hextra50 = $("#hextra50").val();
    var hextra100 = $("#hextra100").val();
      
    var ret25 = 0;
    var ret50 = 0;
    var ret100 = 0;
    
    if(hextra25.length > 1){
        var total25 = $("#totalhe25").val();
        var limite25 = $("#limitehe25").val();
        ret25 = comprobarLimite(total25, hextra25, limite25);
    }
    
    if(hextra50.length > 1){      
        var total50 = $("#totalhe50").val();
        var limite50 = $("#limitehe50").val();
        ret50 = comprobarLimite(total50, hextra50, limite50);        
    }
    
    if(hextra100.length > 1){        
        var total100 = $("#totalhe100").val();
        var limite100 = $("#limitehe100").val();
        ret100 = comprobarLimite(total100, hextra100, limite100);
    }
    
    if((ret25 + ret50 + ret100) < 1){
       $("#frmIngreso").submit(); 
    }
}

function comprobarLimite(total, incremento, limite){
    var lm = limite.split(":");
    var lmhr = parseInt(lm[0]);
    
    var sumahr = totalHoras(total,incremento);

    if(sumahr > lmhr){
//        encerarHExtra();
        alert(sumahr+' pasa del limite de '+lmhr);
        return 1;
    }
    
    return 0;
    
}

function saveReceso(cedula){
    var campo = '#horario'+cedula;
    var receso = $(campo).val();
    var url = ip+'/recesos/actualizar/'+cedula+'/'+receso;
    $.ajax({
        type:"GET",
        url: url,
        success: function(datos){
            //alert(datos);
            $("#mensajes").html(datos);
        }
    });
}

function recalcularSalida(){
    var nroDia = $("#nroDia").val();
    var tipoEmp = $("#tipo_emp").val();
    var salReceso = $("#salReceso").val();
    nroDia = parseInt(nroDia);

    if(nroDia < 6){
        var hsalida = $("#hsalida").val();
        var salida = $("#salida").val();
        hsalida = hsalida.trim();
        salida = salida.trim();
        
        var restaHr = restarHorasSS(salida,hsalida);

        comp = restaHr.split(":");
        comph = parseInt(comp[0]);
        if(comph >=4)
            restaHr = '04:00';

        if(tipoEmp == "01" || tipoEmp == "03" || tipoEmp == "04" || tipoEmp == "17"){
            $("#hextra25").val(restaHr);
        }else{
            $("#hextra50").val(restaHr);
        }
    }else{
        var ingreso = $("#ingreso").val();
        var salida = $("#salida").val();
        ingreso = ingreso.trim();
        salida = salida.trim();
        
        var resta = restarHorasSS(salida,ingreso);

        if(salReceso.length >0){            
            resta = restarHorasSS(resta,salReceso);
        }

        $("#hextra100").val(resta);
    }

}

function recalcularIngreso(){
    var nroDia = $("#nroDia").val();
    var tipoEmp = $("#tipo_emp").val();
    nroDia = parseInt(nroDia);

    var salida = $("#ingreso").val();
    salida = salida.trim();

    sal = salida.split(":");
    hsalida = parseInt(sal[0]);
    if(hsalida < 6){
        hextra = restarHorasSS('06:00',salida);
    }else{
        hextra = '';
    }

    if(nroDia < 6){
        if(tipoEmp == "01" || tipoEmp == "03" || tipoEmp == "04" || tipoEmp == "17"){
            $("#hextra50").val(hextra);
        }else{
            $("#hextra100").val(hextra);
        }
    }else{

        $("#hextra100").val(hextra);
    }

    
}

function todosCheck(){
    var nrocorreos = $("#nrocorreos").val();    
    if($("#todos").is(':checked')){
        var cambio = true;        
    }else{
        var cambio = false;        
    }    
    for(var i=1; i<nrocorreos; i++){
        var check = "#check"+i;
        $(check).prop('checked',cambio);            
    }
    
}


function imprime(){
    $("#imprimir").printArea();
}
