
function busquedaNomina(){
  var buscado = $("#buscado").val();
  buscado = buscado.toUpperCase();
  if(buscado.length > 2){
    var url = ip+'/personal/busqueda/'+buscado;
    $("#tabla").load(url);
  }
}

function busquedaNombre(){
    var buscado = $("#nombre").val();
    buscado = buscado.toUpperCase();
    if(buscado.length > 1){
        var url = ip+'/personal/busquedanombre/'+buscado;
        $("#tabla").load(url);
    }
}

function busquedaCedula(){
    var buscado = $("#cedula").val();
    buscado = buscado.toUpperCase();
    if(buscado.length > 1){
        var url = ip+'/personal/busquedacedula/'+buscado;
        $("#tabla").load(url);
    }
}
function busquedaNombreCampo(campo){
    var cam = "#"+campo;
    var buscado = $(cam).val();
    buscado = buscado.toUpperCase();
    if(buscado.length > 1){
        var url = ip+'/personal/busquedanombrecampo/'+buscado+'/'+campo;
        $("#tabla").load(url);
    }
}
function busquedaCertificados(){
  var buscado = $("#buscado").val();
  buscado = buscado.toUpperCase();
  if(buscado.length > 2){
    var url = ip+'/personal/buscertificados/'+buscado;
    $("#tabla").load(url);
  }
}
function busquedaLlamados(){
  var buscado = $("#buscado").val();
  buscado = buscado.toUpperCase();  
  if(buscado.length > 2){
    var url = ip+'/personal/busllamado/'+buscado;
    $("#tabla").load(url);
  }
}

function datosPersona(cedula,nombre){
    $("#cedula").val(cedula);
    $("#nombre").val(nombre);
    $("#tabla").html("");
}
function datosPersonaCampo(campo,cedula,nombre,cargo){
    var cam = "#"+campo;
    var camcargo = "#"+campo+"cargo"
    $(cam).val(nombre);
    $(camcargo).val(cargo);
    $("#tabla").html("");
}
function selectPersona(cedula, nombre){
  $("#cedula").val(cedula);
  $("#selectNombre").text(nombre);
}

function selectPersona2(cedula, nombre){
  $("#cedula2").val(cedula);
  $("#selectNombre2").text(nombre);
}
function selectPersona3(cedula, nombre){
  $("#cedula3").val(cedula);
  $("#selectNombre3").text(nombre);
}

$(document).ready(function(){
 $("#myModal").modal('show');
 });

 function seleccion(id){
   if(id.length>0){
     var url = ip+'/personal/individual/'+id;
     $("#idPersonal").load(url);
   }
 }

 $(document).ready(function() {
     $(".tabs-menu a").click(function(event) {
         event.preventDefault();
         $(this).parent().addClass("current");
         $(this).parent().siblings().removeClass("current");
         var tab = $(this).attr("href");
         $(".tab-content").not(tab).css("display", "none");
         $(tab).fadeIn();
     });
 });

 function selecSeccion()
 {
   $("#formDireccion").submit();
 }

 function selecTipo()
 {
   $("#formTipoemp").submit();
 }

 function selecHorario()
 {
   $("#formHorario").submit();
 }
