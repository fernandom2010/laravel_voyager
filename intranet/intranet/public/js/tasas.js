/**
 * Created by fernando on 08/08/17.
 */

function validar(){
    var cmp_especie = document.getElementById('especie_estado').value;
    var cmp_tipo = document.getElementById('tipo').value;
    var cmp_cedula = document.getElementById('cedula').value;
    var habilitar = 1;
    var mensaje = "";

    if(vacio('nombre',5) == 1){
        mensaje = "Error en el nombre";
        habilitar = 0;
    }
    if(vacio('cedula',10) == 1){
        mensaje = "Error en el n&uacute;mero de cedula";
        habilitar = 0;
    }
    if(cmp_especie==0){
        mensaje = "Error en el n&uacute;mero de especie";
        habilitar = 0;
    }
    if(cmp_tipo==0){
        mensaje = "Seleccione el tipo de especie";
        habilitar = 0;
    }

    if(habilitar == 1){
        document.form1.submit();
    }else{
        mensaje = '<strong>'+mensaje+'</strong>';
        $("#mensajes").html(mensaje);
        $("#mensajes").show();
    }

}

function comp_cedula(){
    var cedula = $("#cedula").val();
    if(cedula.length>9){
        var url = ip+'/apptasas/datosciudadano/'+cedula;
        $.ajax({
            type:"GET",
            url: url,
            success: function(datos){
                var ciunuevo = 0;
                if(datos.length>1){
                    registros = datos.split("#");
                    var nombre = registros[0];
                    $("#nombres").html(nombre);
                    var nombres = $("#nombres").html();
                    $("#nombre").val(nombres);
                    $("#votacion").val(registros[1]);
                    ciunuevo = registros[2];
                }
                $("#ciudadanonuevo").val(ciunuevo);
            }
        });
    }
}

function comp_especie(){
    var especie = $("#especie").val();
    var tipo = $("#tipo").val();
    var blanco = "";
    var ntipo = parseInt(tipo);
    if(ntipo == 0){
        var mensaje = '<strong>Seleccione el tipo de especie</strong>';
        $("#mensajesError").html(mensaje);
        $("#mensajesError").show();
        $("#mensajesOk").hide();
        return 0;
    }else{
        $("#mensajesError").html(blanco);
        $("#mensajesError").hide();
        $("#mensajesOk").hide();
    }


    $("#mensajes").html(blanco);
    if(especie.length>1){
        var url = ip+'/apptasas/datosespecie/'+tipo+'/'+especie;
        $.ajax({
            type:"GET",
            url: url,
            success: function(datos){
                var comprobar = datos.split("#");
                var estado = comprobar[0];
                estado = parseInt(estado);
                var mensaje = '<strong>'+comprobar[1]+'</strong>';
                if(estado == 1){
                    $("#especie_estado").val(estado);
                    $("#mensajesOk").html(mensaje);
                    $("#mensajesOk").show();
                    $("#mensajesError").hide();
                    document.getElementById("btnGuardar").disabled = false;
                }else{
                    $("#especie_estado").val(0);
                    $("#mensajesError").html(mensaje);
                    $("#mensajesError").show();
                    $("#mensajesOk").hide();
                    $("#btnGuardar").prop('disabled','true');
                    document.getElementById("btnGuardar").disabled = true;
                }


            }
        });
    }
}