function sumarHoras(hora1,hora2){
    
    if(hora1.length > 1)
        var h1Array = hora1.split(":");
    
    if(hora2.length > 1)
        var h2Array = hora2.split(":");
     
    var sumHr =  parseInt(h1Array[0]) + parseInt(h2Array[0]);
    var sumMin = parseInt(h1Array[1]) + parseInt(h2Array[1]);
    var sumSeg = parseInt(h1Array[2]) + parseInt(h2Array[2]);
    
    if(sumSeg > 60){
        sumMin = sumMin + 1;
        sumSeg = sumSeg - 60;
    }
    if(sumMin > 60){
        sumHr = sumHr + 1;
        sumMin = sumMin - 60;
    }
    
    var total = sumHr+":"+sumMin+":"+sumSeg;
    
    return total;
}

function totalHoras(hora1,hora2){
    var ret = sumarHoras(hora1,hora2);
    var retArray = ret.split(":");
    return retArray[0];
}

function restarHoras(hora1, hora2){
    if(hora1.length > 1)
        var h1Array = hora1.split(":");
    
    if(hora2.length > 1)
        var h2Array = hora2.split(":");
     
    var sumHr =  parseInt(h1Array[0]) - parseInt(h2Array[0]);
    var sumMin = parseInt(h1Array[1]) - parseInt(h2Array[1]);
    var sumSeg = parseInt(h1Array[2]) - parseInt(h2Array[2]);
    
    if(sumSeg < 0){
        sumMin = sumMin - 1;
        sumSeg = 60 + sumSeg;
    }
    if(sumMin < 0){
        sumHr = sumHr - 1;
        sumMin = 60 + sumMin;
    }

    if(sumHr < 0)
        return '00:00:00';

    if(sumSeg < 10)
        sumSeg = "0"+ sumSeg;
    if(sumMin < 10)
        sumMin = "0"+ sumMin;
    if(sumHr< 10)
        sumHr = "0"+ sumHr;
    
    var total = sumHr+":"+sumMin+":"+sumSeg;
    
    return total;
    
}

function restarHorasSS(hora1, hora2){
    if(hora1.length > 1)
        var h1Array = hora1.split(":");
    if(hora2.length > 1)
        var h2Array = hora2.split(":");
    
    var sumHr =  parseInt(h1Array[0]) - parseInt(h2Array[0]);
    var sumMin = parseInt(h1Array[1]) - parseInt(h2Array[1]);
    
    if(sumMin < 0){
        sumHr = sumHr -1;
        sumMin = 60 + sumMin;
    }
    if(sumHr < 0)
        return '00:00';
    
    if(sumMin < 10)
        sumMin = "0" + sumMin;
    if(sumHr < 10)
        sumHr = "0" + sumHr;
    
    var total = sumHr+":"+sumMin;
    
    return total;
}
