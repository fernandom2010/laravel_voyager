var gAutoPrint = true; // Flag for whether or not to automatically call the print function

function printSpecial()
{
	if (document.getElementById != null)
	{
		var html = '<HTML>\n<HEAD>\n';

		if (document.getElementsByTagName != null)
		{
			var headTags = document.getElementsByTagName("head");
			if (headTags.length > 0)
				html += headTags[0].innerHTML;
		}

		html += '\n</HE' + 'AD>\n<BODY>\n<table>';

		var printReadyElem = document.getElementById('imprimir');

		if (printReadyElem != null)
		{
				html += printReadyElem.innerHTML;
		}
		else
		{
			alert("No se puede imprimir en la pagina preliminar");
			return;
		}

		html += '</table>\n</BO' + 'DY>\n</HT' + 'ML>';

		var printWin = window.open("","printSpecial");
		printWin.document.open();
		printWin.document.write(html);
		printWin.document.close();
		if (gAutoPrint)
			printWin.print();
	}
	else
	{
		alert("La version de su browser no permite imprimir.");
	}
}

function printArea(area)
{
    if (document.getElementById != null)
    {
        var html = '<HTML>\n<HEAD>\n';

        if (document.getElementsByTagName != null)
        {
            var headTags = document.getElementsByTagName("head");
            if (headTags.length > 0)
                html += headTags[0].innerHTML;
        }

        html += '\n</HE' + 'AD>\n<BODY>\n<table>';

        var printReadyElem = document.getElementById(area);

        if (printReadyElem != null)
        {
            html += printReadyElem.innerHTML;
        }
        else
        {
            alert("No se puede imprimir en la pagina preliminar");
            return;
        }

        html += '</table>\n</BO' + 'DY>\n</HT' + 'ML>';

        var printWin = window.open("","printSpecial");
        printWin.document.open();
        printWin.document.write(html);
        printWin.document.close();
        if (gAutoPrint)
            printWin.print();
    }
    else
    {
        alert("La version de su browser no permite imprimir.");
    }
}
