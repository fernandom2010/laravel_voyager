<?php 
Route::get('/personal/listado', 'Rrhh\PersonalController@listado');
Route::get('/personal/nomina',      'Rrhh\PersonalController@nomina')->middleware('auth');
Route::get('/personal/certificados','Rrhh\PersonalController@certificados')->middleware('auth');
Route::get('/personal/llamado','Rrhh\PersonalController@llamado')->middleware('auth')->name('personal.llamado');
Route::get('/personal/persona/{cedula}','Rrhh\PersonalController@persona');
Route::get('/personal/busqueda/{buscado}','Rrhh\PersonalController@busqueda');
Route::get('/personal/busquedanombre/{buscado}','Rrhh\PersonalController@busquedanombre');
Route::get('/personal/busquedacedula/{buscado}','Rrhh\PersonalController@busquedacedula');
Route::get('/personal/busquedanombrecampo/{buscado}/{campo}','Rrhh\PersonalController@busquedanombrecampo');
Route::get('/personal/buscertificados/{buscado}','Rrhh\PersonalController@buscertificados');
Route::get('/personal/busllamado/{buscado}','Rrhh\PersonalController@busllamado')->name('personal.busllamado');;
Route::get('/personal/individual/{id}','Rrhh\PersonalController@individual')->name('personal.individual');
Route::get('/personal/reportes', 'Rrhh\ReportesController@index')->middleware('auth');
Route::post('/personal/impreporte', 'Rrhh\ReportesController@reporte');
Route::match(['get','post'],'/personal/subir','Rrhh\PersonalController@subir')->name('personal.subirfoto');

Route::get('/personal/correo/{titulo}','Rrhh\CorreosController@index')->name('personal.correo');
Route::post('/personal/enviocorreo','Rrhh\CorreosController@enviar')->name('correo.enviar');
Route::post('/personal/enviocorreos','Rrhh\CorreosController@enviarvarios')->name('correo.enviarvarios');

Route::match(['get','post'],'/personal/inicio{link?}','Rrhh\PersonalController@inicio')->middleware('auth')->name('personal.inicio');

Route::resource('personal','Rrhh\PersonalController');
