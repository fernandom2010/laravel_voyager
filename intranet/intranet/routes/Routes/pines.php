<?php

Route::get('pines','Rrhh\PinesController@index')->name('pines.index');
Route::get('pines/generar','Rrhh\PinesController@generar')->middleware('auth')->name('pines.generar');
Route::get('pines/habilitar/{cedula}','Rrhh\PinesController@habilitar')->middleware('auth')->name('pines.habilitar');
Route::get('pines/exportar','Rrhh\PinesController@exportar')->middleware('auth')->name('pines.exportar');
Route::get('pines/actualizar/{cedula}','Rrhh\PinesController@actualizar')->middleware('auth')->name('pines.actualizar');

//Route::resource('pines','Rrhh\PinesController');
