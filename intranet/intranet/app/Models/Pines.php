<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pines extends Model
{
  protected $table = 'pines';
  protected $fillable = ['pin','cedula','desde','created_at'];
  protected $hidden = ['id'];
}
