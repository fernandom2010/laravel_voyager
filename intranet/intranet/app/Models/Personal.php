<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Personal extends Model
{
  protected $table = 'personal';
  protected $fillable = ['cuenta','nombre','cedula','horario','codigo',
                         'tipo_emp', 'reloj','receso', 'seccion','cargo',
                         'sueldo','estadocivil','nacimiento','sexo',
                         'direccion','ciudad','telefono','ingreso','salida',
                         'date','activo','patronal','tipo_sangre',' mail',
                         'instruccion','ocupacion','cuenta_contable','estado'];
 protected $hidden = ['id'];

 public function cargos(){
      return $this->belongsTo('App\Models\Cargos','cargo','id');
 }

 public function secciones(){
      return $this->belongsTo('App\Models\Secciones','seccion','id');
 }

 public function tipodeemp(){
   return $this->belongsTo('App\Models\Tipoemp','tipo_emp','codigo');
 }

 public function horarios(){
   return $this->belongsTo('App\Models\Horarios','horario','id');
 }
 public function recesos(){
    return $this->belongsTo('App\Models\Recesos','receso','id');
 }

 public function tipossangre(){
     return $this->belongsTo('App\Models\Tiposangre','tipo_sangre','id');
 }

 public function accesos(){
     return $this->hasMany('App\Models\Cargos','cedula','cedula');
 }

 
}
