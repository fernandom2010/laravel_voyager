<?php

namespace App\Http\Controllers\Rrhh;

use Illuminate\Http\Request;

use App\Models\Personal;
use App\Models\Capacitacion;
use App\Models\Formacion;
use App\Models\Cargos;
use App\Models\Titulos;
use App\Models\Especialidad;
use App\Models\Mssql;

use App\Http\Requests;
use App\Http\Controllers\Controller;


class ImportarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */

    public function index()
    {
        $links = array();
        $links['formacion']['title'] = 'Formacion';
        $links['formacion']['link'] = 'Rrhh\ImportarController@formacion';
        $links['cargos']['title'] = 'Cargos';
        $links['cargos']['link'] = 'Rrhh\ImportarController@cargos';
        $links['titulos']['title'] = 'Titulos';
        $links['titulos']['link'] = 'Rrhh\ImportarController@titulos';
        $links['capacitacion']['title'] = 'Capacitacion';
        $links['capacitacion']['link'] = 'Rrhh\ImportarController@formacion';

        //       dd($links);
        return view('personal.importar.index', compact('links'));
    }

    public function personal()
    {
        $origen = new Mssql();
        $personal = $origen->personal();
//dd($personal['1002537221']);
        $listado = Personal::orderBy('nombre')->get();
        foreach ($listado as $persona) {
            $lista[$persona->cedula] = $persona;
        }
        $acciones = [];
        //dd($lista['1002537221']->nombre);
        foreach ($personal as $cedula => $n) {
            if (isset($lista[$cedula])) {   //persona existente => actualizar
                $datax = [];
                $datax['cargo'] = $n['cargo'];
                $datax['sueldo'] = $n['sueldo'];
                $datax['seccion'] = $n['seccion'];
                $datax['codigo'] = $n['codigo'];
                $datax['tipo_emp'] = $n['tipo_emp'];
                $datax['nacimiento'] = $n['nacimiento'];
                $datax['sexo'] = $n['sexo'];
                $datax['direccion'] = $n['direccion'];
                $datax['ciudad'] = $n['ciudad'];
                $datax['telefono'] = $n['telefono'];
                $datax['ingreso'] = $n['ingreso'];
                $datax['salida'] = $n['salida'];
                $datax['mail'] = $n['mail'];
                $datax['instruccion'] = $n['instruccion'];
                $datax['ocupacion'] = $n['ocupacion'];
                $datax['cuenta_contable'] = $n['cuenta_contable'];
                $datax['provincia'] = utf8_encode($n['provincia']);
                $datax['canton'] = utf8_encode($n['canton']);
                $datax['parroquia'] = utf8_encode($n['parroquia']);
                $datax['conyugue'] = utf8_encode($n['conyugue']);
                $datax['padre'] = utf8_encode($n['padre']);
                $datax['madre'] = utf8_encode($n['madre']);
                $datax['notnombre'] = utf8_encode($n['notnombre']);
                $datax['notciudad'] = $n['notciudad'];
                $datax['notdireccion'] = utf8_encode($n['notdireccion']);
                $datax['area'] = $n['area'];
                $datax['unidad'] = $n['unidad'];
                $datax['subunidad'] = $n['subunidad'];
                $datax['ntelf'] = $n['nottelefono'];
                $datax['tipo_sangre'] = $n['tipoSangre'];
                $datax['etnia'] = $n['etnia'];

                Personal::where('cedula', $cedula)->update($datax);
                $acciones[$cedula] = "Actualizado";
            } else {                        //nueva persona
                $datai = [];
                $datai['nombre'] = utf8_encode($n['nombre']);
                $datai['cedula'] = $n['cedula'];
                $datai['horario'] = 1;
                $datai['codigo'] = $n['codigo'];
                $datai['tipo_emp'] = $n['tipo_emp'];
                $datai['estado'] = $n['estado'];
                $datai['nacimiento'] = $n['nacimiento'];
                $datai['sexo'] = $n['sexo'];
                $datai['direccion'] = $n['direccion'];
                $datai['ciudad'] = $n['ciudad'];
                $datai['telefono'] = $n['telefono'];
                $datai['ingreso'] = $n['ingreso'];
                $datai['salida'] = $n['salida'];
                $datai['mail'] = $n['mail'];
                $datai['instruccion'] = $n['instruccion'];
                $datai['ocupacion'] = $n['ocupacion'];
                $datai['cuenta_contable'] = $n['cuenta_contable'];
                $datai['provincia'] = utf8_encode($n['provincia']);
                
                $datai['canton'] = utf8_encode($n['canton']);
                $datai['parroquia'] = utf8_encode($n['parroquia']);
                $datai['conyugue'] = utf8_encode($n['conyugue']);
                $datai['padre'] = utf8_encode($n['padre']);
                $datai['madre'] = utf8_encode($n['madre']);
                $datai['notnombre'] = utf8_encode($n['notnombre']);
                
                $datai['notciudad'] = $n['notciudad'];
                $datai['notdireccion'] = utf8_encode($n['notdireccion']);
                $datai['area'] = $n['area'];
                $datai['unidad'] = $n['unidad'];
                $datai['subunidad'] = $n['subunidad'];
                $datai['tipo_sangre'] = $n['tipoSangre'];
                $datai['etnia'] = $n['etnia'];

                Personal::insert($datai);
                $acciones[$cedula] = "Importar";
            }
        }
//dd($acciones);
        //    dd($listado[0]->cedula);
        //dd($personal);
        //    Personal::truncate();
        //    Personal::insert($personal);
        $listado = Personal::orderBy('nombre')->get();
        return view('personal.importar.personal', compact('listado', 'acciones'));
    }

    public function formacion()
    {
        $personas_cedula = array();
        $personas_cedula = $this->personasCedula();
        
        $origen = new Mssql();
        $listado = $origen->formacion($personas_cedula);
//dd($listado);        
        Formacion::truncate();
        Formacion::insert($listado);
        $listado = Formacion::orderBy('cedula')->with(['nombres','titulos','especialidades'])->get();
//dd($listado['nombres']->nombre); 
        return view('personal.importar.formacion', compact('listado'));
        //dd($listado);
    }


    public function cargos()
    {
        $origen = new Mssql();
        $listado = $origen->cargos();
        Cargos::truncate();
        Cargos::insert($listado);
        $listado = Cargos::orderBy('nombre')->get();
        return view('personal.importar.cargos', compact('listado'));
    }

    public function titulos()
    {
        $origen = new Mssql();
        $listado = $origen->titulos();
        Titulos::truncate();
        Titulos::insert($listado);
        $listado = Titulos::orderBy('nombre')->get();
        return view('personal.importar.titulos', compact('listado'));
    }
    
    public function especialidad()
    {
        $origen = new Mssql();
        $listado = $origen->especialidad();
        Especialidad::truncate();
        Especialidad::insert($listado);
        $listado = Especialidad::orderBy('id')->get();
        return view('personal.importar.especialidad', compact('listado'));
    }

    public function capacitacion()
    {

        $personas_cedula = array();
        $personas_cedula = $this->personasCedula();
        $origen = new Mssql();
        $capacitacion = $origen->capacitacion($personas_cedula);
        Capacitacion::truncate();
        Capacitacion::insert($capacitacion);
        $listado = Capacitacion::get();
        return view('personal.importar.capacitacion', compact('listado'));
        dd($capacitacion);
    }

    private function personasCedula()
    {
        $listado = personal::get();
        $personas_cedula = array();
        foreach ($listado as $item) {
            if (strlen($item->codigo) > 0)
                $personas_cedula[$item->codigo] = $item->cedula;
        }
        return $personas_cedula;
    }

}
