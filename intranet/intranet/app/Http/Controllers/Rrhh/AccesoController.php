<?php

namespace App\Http\Controllers\Rrhh;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Procesos\AccesoProcesos;
use App\Procesos\FechasProcesos;

class AccesoController extends Controller
{
    private $accesoClasificacion;
    private $accesoRecesos;
    
    function __construct()
    {
        $this->accesoClasificacion = array("ingreso","atrazo","receso","adelanto","salida");
        $this->accesoRecesos = array("ingreso","receso","salida");
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $url = 'http://www.gpi.gob.ec/rrhh1/public/index.php';
      //$url = 'http://build.imbabura.gob.ec/gpi-rrhh1/public/index.php';
      return view('homepages.iframes', compact('url'));
    }

    /**
    *   Busca los registros de acceso por fecha y cedula y les
    *   organiza en 5 rangos:
    *       ingreso
    *       atrazo
    *       receso
    *       adelanto
    *       salida
    */
    public function accesoFecha5(Request $request, $cedula, $fecha){

        $accProcesos = new AccesoProcesos();
        $infoPersonas = \App\Models\Personal::where('cedula',$cedula)    
                                            ->with(['horarios','recesos'])
                                            ->get();
        $infoPersona = $infoPersonas[0];                    
       
        $horario = $infoPersona->horarios;      
        $receso = $infoPersona->recesos;
        
        $rangosHorarios = $accProcesos->rangosHorarios($horario,$receso);    

        $registros = $accProcesos->accesoFecha($cedula,$fecha);
   
        $registros = $accProcesos->organizarRegistros($registros,$rangosHorarios);
          
        $accesoClasificacion = $this->accesoClasificacion;
      
        return  view ('acceso.porfecha', compact('accesoClasificacion','registros'));     
    }
    
    /**
    *   Busca los registros de acceso por fecha y cedula y les
    *   organiza en 4 rangos:
    *       ingreso
    *       salida_receso
    *       retorno_receso    
    *       salida
    */
    public function accesoFecha4(Request $request, $cedula, $fecha, $pagadomes, $pagadoano, $impSegundos=1){
        $accProcesos = new AccesoProcesos();
        $fecProcesos = new FechasProcesos();    
        $infoPersona = \App\Models\Personal::where('cedula',$cedula)
                                            ->with(['horarios','recesos','tipodeemp'])                                        
                                            ->first();
                                            
        $regAnterior = \App\Models\Horasextra::where('cedula',$cedula)
                                                    ->where('fecha',$fecha)
                                                    ->where('estado', true)
                                                    ->first(); 
/*
        if(count($regAnterior)>0 ){
            return view('acceso.registroexistente', compact('regAnterior'));
        }                                     
*/
        $alerta = "";
        if(count($regAnterior)>0 ){
            $alerta = "Horas extra pagadas en ".$regAnterior->pagadoano."-".$regAnterior->pagadomes;
        }

        $horario = $infoPersona->horarios;
        $tipo_emp = $infoPersona->tipodeemp->cod;

        $registros = $accProcesos->accesoFecha($cedula,$fecha);

        //Organizar los registros de acuerdo al horario y los recesos
        $receso = $infoPersona->recesos;

        $nroDia = $fecProcesos->setFechaDiaN($fecha);

        if(count($registros)>0) {

            if($nroDia>5){
                $registros = $accProcesos->organizarRegistrosSinHorario($registros);

            }else{
                if (is_object($receso)) {    //si tiene registrado horario de receso
                    $rangosRecesos = $accProcesos->rangosRecesos($receso);                    
                    $registros = $accProcesos->organizarRegistrosReceso($registros, $rangosRecesos);
                } else {     // si no tiene registrado horario de recesos - trabajadores
                    $rangoHorario = $accProcesos->rangosSinRecesos($horario);
                    $registros = $accProcesos->organizarRegistrosSinRecesos($registros, $rangoHorario);
                }
            }
        }

         //Incluir los registros de guardias
  //      $registros = $accProcesos->accesoGuardiasFecha($registros,$cedula,$fecha);

        // CALCULO DE HORAS EXTRA

        $registros['hextra25']='';
        $registros['hextra50']='';
        $registros['hextra100']='';
   //     dd($registros);
        if(count($registros)>3) {
            if ($nroDia > 5) {
                $registros['hextra100'] = $accProcesos->horasExtraFinSemana($registros);
            } else {
                if (strcmp($tipo_emp,"04")==0 || strcmp($tipo_emp, "03") == 0 || strcmp($tipo_emp,"01") == 0 || strcmp($tipo_emp,"17") == 0) {      //empleados trabajadores contratados - incluir a los de LOSEP
                    $registros['hextra25'] = $accProcesos->horasExtraSalida($horario->sal_t, $registros['salida']);
                    $registros['hextra50'] = $accProcesos->horasExtraMadrugada($registros);
                } else {                   //trabajadores
                    $registros['hextra50'] = $accProcesos->horasExtraSalida($horario->sal_t, $registros['salida']);
                    $registros['hextra100'] = $accProcesos->horasExtraMadrugada($registros);
                }
            }
        }

        $registoAnt = \App\Models\Horasextra::where('cedula',$cedula)
                                                ->where('fecha',$fecha)
                                                ->where('estado', TRUE)
                                                ->get();
        if(is_object($registoAnt)){
            foreach($registoAnt as $reg){
                if(strlen($reg->ingreso)>0)
                    $registros['ingreso'] = array($reg->ingreso);

                $rec = array();
                if(strlen($reg->recesosalida)>0)
                    array_push($rec,$reg->recesosalida);
                if(strlen($reg->recesoingreso)>0)
                    array_push($rec,$reg->recesoingreso);
                if(count($rec)>0)
                    $registros['receso'] = $rec;

                if(strlen($reg->salida)>0)
                    $registros['salida'] = array($reg->salida);

                if(strlen($reg->hextra25)>0)
                    $registros['hextra25'] = $reg->hextra25;
                if(strlen($reg->hextra50)>0)
                    $registros['hextra50'] = $reg->hextra50;
                if(strlen($reg->hextra100)>0)
                    $registros['hextra100'] = $reg->hextra100;

                $registros['id'] = $reg->id;
                if($reg->pagadomes != $pagadomes){
                    $registros['habilitado'] = 'Horas extras pagadas en '.$reg->pagadomes.'/'.$reg->pagadoano;
                }

            }
        }

//  dd($registros);
        // Registros de pagos anteriores en esa fecha

        $fechaArray = explode('-',$fecha);
        $mes = $fechaArray[1];        
        $desde = date("Y-m-d", mktime(0, 0, 0, $mes, 1, $fechaArray[0]));
        $hasta = date("Y-m-d", mktime(0, 0, 0, $mes+1, 0, $fechaArray[0]));
        if(strlen($pagadomes)==1) $pagadomes = '0'.$pagadomes;

        $totalhextra = array();
        $limiteHorasExtra = array();

//dd($registros);
        $registrosAnt = \App\Models\Horasextra::where('cedula',$cedula)
                                                ->where('fecha','>',$desde)
                                                ->where('fecha','<',$hasta)
//                                                ->where('pagadomes',$pagadomes)
//                                                ->where('pagadoano',$pagadoano)
                                                ->where('estado',TRUE)
                                                ->get();


//dd($registrosAnt);
//  dd($registros);
        $totalhextra = $accProcesos->sumarHorasExtra($registrosAnt);
        $limiteHorasExtra = $accProcesos->maximoHorasExtra($infoPersona->tipodeemp->cod,$infoPersona->horarios->nombre, $fechaArray[1]);
//dd($limiteHorasExtra);
        $accesoRecesos = $this->accesoRecesos; 
        if($impSegundos > 0){
            return view('acceso.porrecesos', compact('horario','receso','accesoRecesos','registros',
                                            'registrosAnt','regAntNro','totalhextra','limiteHorasExtra','alerta'));
        }else{
            return view('acceso.porrecesossinseg', compact('horario','receso','accesoRecesos','registros',
                                            'registrosAnt','regAntNro','totalhextra','limiteHorasExtra','alerta'));
        }                                    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
