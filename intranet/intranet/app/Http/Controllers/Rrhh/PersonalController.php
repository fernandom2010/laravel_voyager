<?php

namespace App\Http\Controllers\Rrhh;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Procesos\PersonalProcesos;
use App\Procesos\FechasProcesos;

class PersonalController extends Controller
{

    public function index(Request $request)
    {
      $this->authorize('index', new \App\Models\Personal);

      ($request->has('seccionSeleccionada'))? $seccion = $request->input('seccionSeleccionada'):$seccion = 0;
      ($seccion == 0)? $qseccion = '>':$qseccion='=';

      ($request->has('tipoSeleccionada'))? $tipoemp = $request->input('tipoSeleccionada'):$tipoemp = 0;
      ($tipoemp == 0)? $qtipoemp = '>':$qtipoemp='=';

      ($request->has('tipoHorario'))?$horario = $request->input('tipoHorario'):$horario = 0;
      ($horario == 0)? $qhorario = '>':$qhorario = '=';



      $listaPersonal = \App\Models\Personal::where('estado','>','0')
                                       ->where('seccion',$qseccion,$seccion)
                                       ->where('tipo_emp',$qtipoemp, $tipoemp)
                                       ->where('horario', $qhorario, $horario)
                                       ->where('horario','!=','5')
                                       ->where('activo','=','1')
                                       ->orderBy('nombre')->with(['secciones'])->get();
//      $lpersonal = \App\Models\Secciones::orderBy('nombre')->get();
      $lsecciones = \App\Models\Secciones::orderBy('nombre')->get();
      $ltiposemp =  \App\Models\Tipoemp::orderBy('nombre')->get();
      $lhorarios =  \App\Models\Horarios::where('activo','1')
                                            ->orderBy('nombre')
                                            ->get();
//dd($lhorarios);
      return view('personal.index',compact('lsecciones','seccion','tipoemp','horario',
                                              'ltiposemp','lhorarios','listaPersonal'));
    }

    public function listado(){
      $listaPersonal = "lista de personal";

      return view('personal.borrar', compact('listaPersonal'));
    }

    public function inicio(Request $request)
    {
      $this->authorize('index', new \App\Models\Personal);

      ($request->has('seccionSeleccionada'))? $seccion = $request->input('seccionSeleccionada'):$seccion = 0;
      ($seccion == 0)? $qseccion = '>':$qseccion='=';

      ($request->has('tipoSeleccionada'))? $tipoemp = $request->input('tipoSeleccionada'):$tipoemp = 0;
      ($tipoemp == 0)? $qtipoemp = '>':$qtipoemp='=';

      ($request->has('tipoHorario'))?$horario = $request->input('tipoHorario'):$horario = 0;
      ($horario == 0)? $qhorario = '>':$qhorario = '=';
      
      ($request->has('cesantes'))?$cesantes='500':$cesantes='5';


      $listaPersonal = \App\Models\Personal::where('seccion',$qseccion,$seccion)
                                       ->where('tipo_emp',$qtipoemp, $tipoemp)
                                       ->where('horario', $qhorario, $horario)
                                       ->where('horario','!=',$cesantes)
                                       ->where('horario','!=','29')
                                       ->where('horario','!=','23')
                                       ->where('horario','!=','17')
                                       ->where('estado','=','1')
                                       ->orderBy('nombre')->with(['secciones','tipossangre'])->get();


      $lsecciones = \App\Models\Secciones::orderBy('nombre')->get();
      $datosSeccion="";
      if($seccion >0)
        $datosSeccion = \App\Models\Secciones::where('id',$seccion)->first();


      $ltiposemp =  \App\Models\Tipoemp::where('activo',TRUE)
                                            ->orderBy('nombre')->get();
      $datosTipo="";
      if($tipoemp>0)
          $datosTipo = \App\Models\Tipoemp::where('codigo',$tipoemp)->first();


      $lhorarios =  \App\Models\Horarios::where('activo','1')
                                            ->orderBy('nombre')
                                            ->get();
      $datosHorario="";
      if($horario>0)
          $datosHorario = \App\Models\Horarios::where('id',$horario)->first();

      return view('personal.index',compact('lsecciones','seccion','datosSeccion','tipoemp','datosTipo','horario', 'cesantes',
                                            'datosHorario', 'ltiposemp','lhorarios','listaPersonal'));
    }

    public function busqueda(Request $request, $buscado= "")
    {
       $buscado = $buscado."%";
       $listaPersonal = \App\Models\Personal::where('nombre','like',$buscado)
 //                                             ->where('estado','>','0')
                                              ->orderBy('nombre')
                                              ->with(['secciones'])->get();
       //$listaPersonal = personal::where('nombre','like',$buscado)->where('estado','>','0')->orderBy('nombre')->with(['cargos'])->get();
       return view('personal.busqueda', compact('listaPersonal'));
    }

    public function busquedanombre(Request $request, $buscado= "")
    {
        $buscado = $buscado."%";
        $listaPersonal = \App\Models\Personal::where('nombre','like',$buscado)
            ->where('estado','>','0')
            ->orderBy('nombre')
            ->with(['secciones'])->get();
        //$listaPersonal = personal::where('nombre','like',$buscado)->where('estado','>','0')->orderBy('nombre')->with(['cargos'])->get();
        return view('personal.busquedanombrecedula', compact('listaPersonal'));
    }
    public function busquedanombrecampo(Request $request, $buscado= "", $campo="")
    {
        $buscado = $buscado."%";
        $listaPersonal = \App\Models\Personal::where('nombre','like',$buscado)
            ->where('estado','>','0')
            ->where('cargo','>','0')
            ->orderBy('nombre')
            ->with(['secciones','cargos'])->get();
        //$listaPersonal = personal::where('nombre','like',$buscado)->where('estado','>','0')->orderBy('nombre')->with(['cargos'])->get();
        return view('personal.busquedanombrecedulacampo', compact('listaPersonal','campo'));
    }
    public function busquedacedula(Request $request, $buscado= "")
    {
        $buscado = $buscado."%";
        $listaPersonal = \App\Models\Personal::where('cedula','like',$buscado)
            ->where('estado','>','0')
            ->orderBy('nombre')
            ->with(['secciones'])->get();
        //$listaPersonal = personal::where('nombre','like',$buscado)->where('estado','>','0')->orderBy('nombre')->with(['cargos'])->get();
        return view('personal.busquedanombrecedula', compact('listaPersonal'));
    }

    public function individual(Request $request, $id="")
    {
      $persona = \App\Models\Personal::where('id',$id)->with(['cargos'])->get();
      $persona = $persona[0];
      $perProcesos = new PersonalProcesos();
      $perFechas = new FechasProcesos();
      
      $formacion = $perProcesos->formacion($persona->cedula, $persona->sexo);
//dd($formacion);      
      $fec_nacimiento = $persona->nacimiento;
      $nacimiento = $perProcesos->calcularEdad($fec_nacimiento);
      $fec_ingreso = $persona->ingreso;
      
      
      $horario = $persona->horario;
      $tipo = '';
      
      $tmp_trabajo = "";
      $fec_salida = $persona->salida;
      if(strcmp($horario,"5")==0){        
        $tmp_trabajo = $perFechas->restarFechas($fec_ingreso,$fec_salida);        
      }else{
        $tipo = $persona->tipo_emp;      
        if(($tipo !=6) && ($tipo !== 3) && ($tipo != 10)){
            $tmp_trabajo = $perProcesos->calcularEdad($fec_ingreso);
        }      
      }
      if(strcmp(substr($fec_salida,0,4),'1900')==0)
        $fec_salida = '-';
        
      $codigo = $perProcesos->codigoTrabajo($persona->horario);        
      $denominacion = $perProcesos->denominacion($persona->sueldo, $codigo);
            
      return view('personal.individual', compact('persona', 'denominacion', 'formacion','nacimiento','tmp_trabajo','fec_salida','id'));
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function nomina()
    {
        $lpersonal = \App\Models\Personal::where('estado','>','0')->orderBy('nombre')->get();
//dump($lpersonal);
        //foreach($listaPersonal as $personal){
        //    var_dump($personal->nombre);
        //}
        //dd($listaPersonal);
        return view('personal.nomina',compact($lpersonal));
    }
    public function certificados()
    {
        $listaPersonal = \App\Models\Personal::where('cargo','>','0')->orderBy('nombre')->with(['cargos'])->paginate(15);
        return view('personal.certificados',compact('listaPersonal'));

    }    
    public function buscertificados(Request $request, $buscado= "")
    {
        $buscado = $buscado."%";
        $listaPersonal = \App\Models\Personal::where('nombre','like',$buscado)->orderBy('nombre')->with(['cargos'])->get();
        return view('personal.buscertificados',compact('listaPersonal'));
    }
    public function llamado()
    {
        $perFechas = new FechasProcesos();        
        
        $listaPersonal = \App\Models\Personal::where('cargo','>','0')
                                                ->where('horario','!=','29')
                                                ->where('horario','!=','23')
                                                ->where('horario','!=','17')
                                                ->where('estado','=','1')
                                                ->orderBy('nombre')->with(['cargos'])->paginate(15);
        $fecha = date('Y-m-d');
        $mes = date('m');
        $meses = $perFechas->setMeses();
//dd($meses);        
        return view('personal.llamados',compact('listaPersonal','fecha','meses','mes'));
    }
    public function busllamado(Request $request, $buscado= "")
    {
        $buscado = $buscado."%";
        $listaPersonal = \App\Models\Personal::where('nombre','like',$buscado)->orderBy('nombre')->with(['cargos'])->get();
        return view('personal.busllamados',compact('listaPersonal'));
    }    
    public function persona(Request $request, $cedula= "")
    {
        $persona = \App\Models\Personal::where('cedula','=',$cedula)->with(['cargos'])->get();
        $persona = $persona[0];
        $nombre = $persona->nombre;
        $cargo = $persona->cargos->nombre;

        return view('personal.persona', compact('persona'));
        dd($cargo);
    }

    public function anterior()
    {
      $url = 'http://www.gpi.gob.ec/rrhh1/public/index.php/Nomina';
      //$url = 'http://build.imbabura.gob.ec/gpi-rrhh1/public/index.php/Nomina';
      return view('homepages.iframes', compact('url'));
    }

    public function anterior0()
    {
      $url = 'http://www.gpi.gob.ec/rrhh/acceso.php';
      //$url = 'http://build.imbabura.gob.ec/gpi-rrhh/acceso.php';
      return view('homepages.iframes', compact('url'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cargos =    \App\Models\Cargos::orderBy('nombre')->get();
        $secciones = \App\Models\Secciones::orderBy('nombre')->get();
        $tipodeemp = \App\Models\Tipoemp::orderBy('nombre')->get();
        $horarios =  \App\Models\Horarios::where('activo','1')->orderBy('nombre')->get();

        return view('personal.create', compact('cargos','secciones','tipodeemp','horarios'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(\App\Http\Requests\StorePersonalPostRequest $request) {
    //    $this->authorize('create', new \App\Models\Role);
      $nombre = $request->input('nombre');
      $nombre = strtoupper($nombre);
      $tipo_emp_id = $request->input('tipo_emp');
      $tipoemp = \App\Models\Tipoemp::where('id',$tipo_emp_id)->get();
      $codigo = $tipoemp[0]->cod;
      $tipo_emp = $tipoemp[0]->codigo;

      $personal = new PersonalProcesos();
      $cuenta = $personal->generarCuenta($codigo,$nombre);

      $datos = $request->only(['nombre', 'cedula', 'tipo_emp', 'cargo','seccion', 'horario']);
      $datos['nombre'] = $nombre;
      $datos['cuenta'] = $cuenta;
      $datos['tipo_emp'] = $tipo_emp;
//dd($datos);
      $personal = \App\Models\Personal::create($datos);
      return redirect(route('personal.inicio'));
    }

    public function subir(Request $request)
    {   
        $foto = $request->file('file');
        $nombre = $request->input('cedula');
        $nombre = $nombre.'.JPG';
        $link = "#".$request->input('cedula');
        $id = $request->input('id');
        
        $destino = '../public/img/personal';
        $resp = $foto->move($destino,$nombre);
        return redirect()->route('personal.inicio',['link'=>$link]);
 //       dd($resp);
 //       dd('subido');
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
