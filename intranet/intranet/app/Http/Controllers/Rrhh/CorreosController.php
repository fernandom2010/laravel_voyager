<?php

namespace App\Http\Controllers\Rrhh;

use Illuminate\Http\Request;

use Mail;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Contracts\Mail\Mailer as MailerContract;


class CorreosController extends Controller
{
    protected $mailer;

    public function __construct(MailerContract $mailer){
        $this->mailer = $mailer;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($titulo)
    {
        $message = [
            'title' => $titulo,
            'intro' => "Please verify your email address with ",
            'link' => '',
            'confirmation_code' => '',
            'to_email' => 'fminio@imbabura.gob.ec',
            'to_name' => 'Fernando Miño',
        ];
    /*    
        dd($message);

        \Mail::send('emails.reminder', $message,
            function ($m) use ($message) {
                $m->to($message['to_email'], $message['to_name'])
                    ->subject('Email verification');
            });
            
    */
    
        return $this->mailer->send('emails.reminder', $message,
            function ($m) use ($message) {
                $m->to($message['to_email'], $message['to_name'])
                    ->subject('Email verification');
            });

    }
    
    public function enviar(Request $request)
    {
        $message = [
            'title' => $request->input('titulo'),            
            'body' => $request->input('contenido'),
            'to_email' => $request->input('correo'),
            'to_name' => 'REPORTE PAGO DE HORAS EXTRA',
        ];
        $mensaje = $request->input('contenido');
        
        \Mail::send('emails.informarHE', ['mensaje'=>$mensaje],
            function ($m) use ($message) {
                $m->to($message['to_email'], $message['to_name'])
                    ->subject($message['title']);
            });  
            
        return view('horasextra.email', compact('mensaje'));
    }
    
    public function enviarvarios(Request $request)
    {
  //dd($request->input('nrocorreos'));  
        $nro = $request->input('nrocorreos');
        $correos = array();
//dd($request);
        for($i = 1; $i<$nro; $i++){
            $comp = 'check'.$i;

            $comprobar = $request->input($comp);
 //           dd($comprobar);
            if($comprobar) {
                $texto = "text".$i;
                $destino = "correo".$i;
                $correos[$i]['mensaje'] = $request->input($texto);
                $correos[$i]['destinatario'] = $request->input($destino);
                $destinatario = $request->input($destino);
//dd($destinatario);
                $message = [
                    'title' => 'REPORTE DE PAGO DE HORAS EXTRA',
                    'body' => $request->input($texto),
                    'to_email' => $destinatario,
                    'to_name' => 'REPORTE PAGO DE HORAS EXTRA',
                ];
                $mensaje = $request->input($texto);


                \Mail::send('emails.informarHE', ['mensaje' => $mensaje],
                    function ($m) use ($message) {
                        $m->to($message['to_email'], $message['to_name'])
                            ->subject($message['title']);
                    });
            }
            
        }
        $mesaje="Correos enviados";
        return view('horasextra.emails', compact('correos'));
 //   dd($correos);    
    }
}
