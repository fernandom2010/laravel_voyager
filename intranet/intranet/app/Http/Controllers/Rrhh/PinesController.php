<?php

namespace App\Http\Controllers\Rrhh;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class PinesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//      $this->authorize('index', new \App\Models\Pines);

      $listadoPersonal = \App\Models\Personal::where('estado','>','0')->orderBy('nombre')->get();
      $listadoPines = \App\Models\Pines::all();

      $listaPines = array();
      foreach($listadoPines as $pin){
        $listaPines[$pin->cedula]['pin'] = $pin->pin;
        $listaPines[$pin->cedula]['activo'] = $pin->activo;
      }
      $i=1;
      foreach($listadoPersonal as $persona){
        $activo = 0;
        $pin = '';
        if(isset($listaPines[$persona->cedula])){
          $activo = $listaPines[$persona->cedula]['activo'];
          $pin = $listaPines[$persona->cedula]['pin'];
        }

        $listaPersonal[$persona->cedula]=array(
          'nro'=>$i,
          'nombre' => $persona->nombre,
          'activo' => $activo,
          'pin'    => $pin
        );
        $i++;
      }
      return view('pines.index',compact('listaPersonal'));
    }

    public function exportar()
    {
      $listadoPines = \App\Models\Pines::where('activo','>','0')->get();
      return view('pines.exportar',compact('listadoPines'));
    }



    public function generar()
    {
      $listadoPersonal = \App\Models\Personal::where('estado','>','0')->orderBy('nombre')->get();
      $listadoPines = \App\Models\Pines::get();

      $listaPersonal = array();
      $listaPines = array();
      foreach($listaPines as $pin){
        $listaPines[$pin->pin] = $pin->cedula;
      }
      foreach($listadoPersonal as $persona){
        $cedula = $persona->cedula;
        if(!isset($listaPersonal[$cedula])){
          $listaPersonal[$cedula]['nombre'] = $persona->nombre;
          $generado = $this->generarPin($listaPines);
          $listaPines[$generado] = $persona->cedula;

          $pines = new \App\Models\Pines;
          $pines->pin = $generado;
          $pines->cedula = $persona->cedula;
          $pines->save();

          $listaPersonal[$persona->cedula]['pin'] = $generado;
        }
      }
      return view('pines.generar',compact('listaPersonal'));
    }

    public function actualizar(Request $request, $cedula= "")
    {
      $listadoPines = \App\Models\Pines::get();
      $listaPines = array();
      $pinAnterior = 0;
      foreach($listadoPines as $pin){
        $listaPines[$pin->pin] = $pin->cedula;
        $pinCedula = $pin->cedula;
        if(strcmp($cedula,$pinCedula)==0){
          $pinAnterior = $pin->pin;
          $pinActualizacion = $pin->updated_at;
        }
      }

      $generado = $this->generarPin($listaPines);
      \App\Models\Pines::where('cedula','=',$cedula)
                          ->update(['pin'=>$generado]);
      $datosPin = \App\Models\Pines::where('cedula','=',$cedula)->get();
      $datosPersona = \App\Models\Personal::where('cedula','=',$cedula)->get();

      $datosPin = $datosPin[0];
      $datosPersona = $datosPersona[0];

      $pinesArchivo = new \App\Models\Pinesarchivo;
      $pinesArchivo->pin = $pinAnterior;
      $pinesArchivo->cedula = $cedula;
      $pinesArchivo->desde = $pinActualizacion;
      $pinesArchivo->hasta = date('Y-m-d h:i:s');
      $pinesArchivo->save();



      return view('pines.actualizar',compact('datosPersona','datosPin','pinAnterior'));
    }

    private function generarPin($listaPines){
      $encontrado = 0;
      do{
        $gen = rand(1000,9999);
        if(isset($listaPines[$gen])){
          $encontrado = 1;
        }else{
          $encontrado = 0;
        }
      }while($encontrado == 1);
      return $gen;
    }

    public function habilitar(Request $request, $cedula= ""){
        \App\Models\Pines::where('cedula','=',$cedula)
                            ->update(['activo'=>1]);
        return redirect('\pines');

    }
}
