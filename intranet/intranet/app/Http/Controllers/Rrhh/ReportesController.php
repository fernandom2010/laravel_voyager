<?php

namespace App\Http\Controllers\Rrhh;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Procesos\PersonalProcesos;


class ReportesController extends Controller
{
    private $campos;

    public function __construct()
    {
      $this->campos = array(
        'nro'=>'Nro', 'foto'=>'Foto', 
        'nombre'=>'Nombre', 'cedula'=>'Cedula', 
        'cargo'=>'Cargo', 'cuenta'=>'Cta Reloj', 
        'seccion'=>'Direccion', 'sueldo'=>'Sueldo', 
        'partida'=>'Part. Presupuestaria' , 'nacimiento'=>'Fecha de Nacimiento', 
        'edad'=>'Edad', 'sexo'=>'Genero',
        'ciudad'=>'Ciudad', 'direccion'=>'Domicilio',
        'telefono'=>'Telefono', 'mail'=>'Mail', 
        'extension'=>'extension', 'ingreso'=>'Fecha de Ingreso',
        'servicio'=>'A&ntilde;os de Servicio', 'salida'=>'Fecha Salida',
        'firma'=>'Firma');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $this->authorize('index', new \App\Models\Personal);
      $secciones = \App\Models\Secciones::orderBy('nombre')->get();
      $tipodeemp = \App\Models\Tipoemp::orderBy('nombre')->get();
      $horarios =  \App\Models\Horarios::orderBy('nombre')->get();
      $campos = $this->campos;

      return view('personal.reportes.index',
                  compact('secciones','tipodeemp','horarios', 'campos'));
    }

    public function reporte(\App\Http\Requests\StoreReportePostRequest $request)
    {
      $this->authorize('index', new \App\Models\Personal);
      
      $encabezado['titulo'] = strtoupper($request->input('titulo'));

      $tipoemp = array('id' => 0,'query' => '>');
      if($request->has('tipo_emp') && $request->input('tipo_emp') >0){
        $tipemp = \App\Models\Tipoemp::where('codigo',$request->input('tipo_emp'))->first();
        $tipoemp = array('id'=>$tipemp->codigo,
                            'nombre'=>$tipemp->nombre,
                            'query'=>'=');
      }

      $departamento = array('id'=>0,'query'=>'>');
      if($request->has('departamento') && $request->input('departamento')>0){
          $sec = \App\Models\Secciones::where('id',$request->input('departamento'))->first();
          $departamento = array('id'=>$sec->id,
                                'nombre'=>$sec->nombre,
                                'query'=>'=');
      }

      $horario = array('id'=>0,'query'=>'>');
//dd($request->has('horario'));
      if($request->has('horario') && $request->input('horario') >0){
          $hr =  \App\Models\Horarios::where('id',$request->input('horario'))->first();
          $horario = array('id'=>$hr->id,
                        'nombre'=>$hr->nombre,
                        'query'=>'=');
      }

      $campos = array();
      foreach($this->campos as $i=>$n){
        if($request->has($i)){
          $campos[$i] = $n;
        }
      }

      $listado = \App\Models\Personal::where('estado',1)
                                        ->where('tipo_emp',$tipoemp['query'], $tipoemp['id'])
                                        ->where('seccion', $departamento['query'], $departamento['id'])
                                        ->where('horario', $horario['query'], $horario['id'])
                                        ->orderBy('nombre')
                                        ->with(['secciones','cargos','tipodeemp','horarios'])
                                        ->get();
//dd($listado);
      $personalProcesos = new PersonalProcesos();
      $nro = 1;
      $tfem = 0;
      $tmas = 0;
      $ttdir = array();

      foreach($listado as $persona){
        $cedula = $persona->cedula;
        foreach($this->campos as $i=>$c){
          switch ($i) {
            case 'nro':
              $lista2[$cedula]['nro'] = $nro;
              $nro++;
              break;
            case 'firma':
              $lista2[$cedula]['firma'] = "<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ".
					      "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".
					      "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</br>";					 

              break;
            case 'foto':
              $lista2[$cedula]['foto'] = '<img src="/img/personal/'.$persona->cedula.'.JPG" width="50px"/>';
              break;
            case 'cargo':
              $lista2[$cedula]['cargo'] = $persona->cargos['nombre'];
              break;
            case 'seccion':
              $ndir = $persona->secciones['nombre'];
              $lista2[$cedula][$i] = $ndir;
              (isset($ttdir[$ndir]))?$ttdir[$ndir]=$ttdir[$ndir]+1:$ttdir[$ndir] = 1;
              break;
            case 'sueldo':
              $lista2[$cedula][$i] = sprintf("%01.2f",$persona->sueldo);
              break;
            case 'sexo':
              ($persona->$i==1)?$lista2[$cedula][$i]="Femenino":$lista2[$cedula][$i]="Masculino";
              ($persona->$i==1)?$tfem++:$tmas++;
              break;
            case 'edad':
              $nacimiento = $persona->nacimiento;
              $lista2[$cedula][$i] = $personalProcesos->calcularEdad($nacimiento);
              break;
            case 'servicio':
              $ingreso = $persona->ingreso;
              $lista2[$cedula][$i] = $personalProcesos->calcularEdad($ingreso);
              break;
            case 'partida':
              $par = '';
              switch($tipoemp['id']){
                case '4':
                  $par = '01.01.01.01.510105.000.10.01.000.000';
                  if(strcmp($persona->seccion,'111') == 0){  $par = '03.01.01.01.710105.000.10.01.000.000'; }
                  if(strcmp($persona->seccion,'121') == 0){  $par = '04.01.01.01.710105.000.10.01.000.000'; }
                break;
                case '5':
			            $par = '01.01.01.01.510106.000.10.01.000.000';
			            if(strcmp($persona->seccion,'121') == 0 ){  $par = '04.01.01.01.710106.000.10.01.000.000'; }
		  	        break;
		  			  	case '1':
			             $par = '01.01.01.01.510510.000.10.01.000.000';
			             if(strcmp($persona->seccion,'111') == 0 ){  $par = '03.01.01.01.710510.000.10.01.000.000'; }
			             if(strcmp($persona->seccion,'121') == 0 ){  $par = '04.01.01.01.710510.000.10.01.000.000'; }
		  	           break;
		  			  	case '2':
			             $par = '01.01.01.01.510503.000.10.01.000.000';
			             if(strcmp($persona->seccion,'121') == 0 ){  $par = '04.01.01.01.710503.000.10.01.000.000'; }
		  	           break;
              }
              $lista2[$cedula][$i] = $par;
              break;

            default:
              $lista2[$cedula][$i] = $persona->$i;
              break;
          }
        }
      }
//dd($lista2);
        $personal = $lista2;
      return view('personal.reportes.reporte',compact('personal','campos',
                                                'encabezado','tipoemp','horario','departamento'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
