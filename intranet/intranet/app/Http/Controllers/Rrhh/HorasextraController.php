<?php

namespace App\Http\Controllers\Rrhh;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Procesos\FechasProcesos;
use App\Procesos\AccesoProcesos;
use App\Models\Horasextra;

class HorasextraController extends Controller
{

    private $años;
    private $meses;
    private $tipos_col;
    protected $user;

    function __construct()
    {
        $fecProcesos = new FechasProcesos();
        $this->meses = $fecProcesos->setMeses();
        $this->años = $fecProcesos->setAños();
        $this->user = Auth::user();
        $this->tipos_col = array(
            "02"=>array(
                'suple' => 2,
                'extra' => 1,
                25=> 'Nocturna <br/>25%',
                50=> 'Normal <br/>50%',
                '25l'=> 'Nocturna 25%',
                '50l'=> 'Normal 50%',
            ),
            "01"=>array(
                'suple' => 1,
                'extra' => 2,
                25 => '25%',
                50 => '60%',
                '25l' => '25%',
                '50l' => '60%'
            )
        );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
        $años = $this->años;
        $meses = $this->meses;
        $datos = array('mes'=>date('m'),'año'=>date('Y'));
        //$datos = array('mes'=>'08','año'=>date('Y'));

        $tipo_emp = 0;

        return view('horasextra.index', compact('datos','años','meses','tipo_emp'));

    }

    public function ingreso(Request $request)
    {
        $años = $this->años;
        $meses = $this->meses;
        $datos = array();
        $accProcesos = new AccesoProcesos;
        $fecProcesos = new FechasProcesos();
        $tipo_emp = 0;
        $horario = array();
        $receso = array();
        $impHorario = array('ing_m'=>'','rec_sal'=>'','rec_ing'=>'','sal_t'=>'');
        $txHorario = '';
        
        if($request->has('cedula')){      
            $datos = $request->only('cedula','nombre','mes','año'); 
            $infoPersonal = \App\Models\Personal::where('cedula',$datos['cedula'])
                                                    ->with(['tipodeemp','horarios','recesos'])
                                                    ->first();
  //          $infoPersonal = $infoPersonal[0];
            $datos['nombre'] = $infoPersonal->nombre;
            $tipo_emp = $infoPersonal->tipodeemp->cod;
            $horario = $infoPersonal->horarios;
            $receso = $infoPersonal->recesos;

            $txHorario = substr($horario->ing_m,0,5).'$';
            $impHorario['ing_m'] = substr($horario->ing_m,0,5);
            if(is_object($receso)){
                $txHorario = $txHorario.substr($receso->salida,0,5).'$';
                $impHorario['rec_sal'] = substr($receso->salida,0,5);
                $txHorario = $txHorario.substr($receso->ingreso,0,5).'$';
                $impHorario['rec_ing'] = substr($receso->ingreso,0,5);
            }else{
                $txHorario = $txHorario.' $ $';
            }
            $txHorario = $txHorario.substr($horario->sal_t,0,5);
            $impHorario['sal_t'] = substr($horario->sal_t,0,5);
 //           $limiteHorasExtra = $accProcesos->maximoHorasExtra($infoPersonal->tipodeemp->cod,$infoPersonal->horarios->nombre);
                                                    
        }

        /**
        *   Registro de horas extra.
        */
        if($request->has('fecha')){         
          
            $hextra = new Horasextra;
            $hextra->cedula = $request->cedula;
            $hextra->fecha = $request->fecha;
            if(strlen($request->ingreso) > 1)       $hextra->ingreso =          $fecProcesos->formatoHora($request->ingreso);
            if(strlen($request->recesosalida) > 1)  $hextra->recesosalida =     $fecProcesos->formatoHora($request->recesosalida);
            if(strlen($request->recesoingreso) > 1) $hextra->recesoingreso =    $fecProcesos->formatoHora($request->recesoingreso);
            if(strlen($request->salida) > 1)        $hextra->salida =           $fecProcesos->formatoHora($request->salida);
            if(strlen($request->hextra25) > 1)      $hextra->hextra25 =         $fecProcesos->formatoHora($request->hextra25);
            if(strlen($request->hextra50) > 1)      $hextra->hextra50 =         $fecProcesos->formatoHora($request->hextra50);
            if(strlen($request->hextra100) > 1)     $hextra->hextra100 =        $fecProcesos->formatoHora($request->hextra100);
        
            $hextra->pagadoano = $request->pagadoano;
            $hextra->pagadomes = $request->pagadomes;
            $hextra->horario = $request->horario;
            $hextra->creadopor = ''.$this->user->name;
            
            $hextra->save();           
             
        }        
    
        /**
        *   Generar reporte de horas extra
        */
        $registroshex = '';
        $totalhextra = array();
        $totalOlympo = array();
        $nombreDias = array();
        $registros = array();
        
        if($request->has('cedula')){
            $totalhextra = array();
            $fecProcesos = new FechasProcesos();
            $registroshex = \App\Models\Horasextra::where('pagadomes',$request->mes)
                                                    ->where('pagadoano',$request->año)
                                                    ->where('cedula',$request->cedula)
                                                    ->where('estado',TRUE)
                                                    ->orderby('fecha','desc')
                                                    ->get();

            foreach($registroshex as $reg){
                $nombreDias[$reg->fecha] = substr($fecProcesos->setFechaDia($reg->fecha),0,2);
                $fechaArray = explode('-',$reg->fecha);
                //$mes = $fecProcesos->setNombreMes($fechaArray[1]);
                //$registros[$mes][$reg->id] = $reg;
                $registros[$reg->id] = $reg;
                if(strcmp('01',substr($reg->fecha,-2))==0){
                    if(strlen($reg->ingreso)<1 && strlen($reg->salida)<1){
                        $registros[$reg->id]['todo'] = 'TODO EL MES';
                    }
                }
            }

            $totalhextra = $accProcesos->sumarHorasExtra($registros);
            $totalOlympo = $accProcesos->totalHorasExtaOlympo($totalhextra);
 //dd($totalOlympo);
/*
            foreach($registros  as $tmpmes=>$listado){
                $totalhextra[$tmpmes] = $accProcesos->sumarHorasExtra($listado);
            }
*/
            if($tipo_emp == '01' || $tipo_emp == '04' || $tipo_emp == '17'|| $tipo_emp == '03'){
                $col = $this->tipos_col["01"];
            }else{
                $col = $this->tipos_col["02"];
            }

        }

        return view('horasextra.ingreso', compact('años','meses','datos','registros','nombreDias','totalhextra', 'totalOlympo', 'tipo_emp', 'col_suple','col','txHorario','impHorario'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {    
        $datos['cedula'] = $request->input('cedula');
        $datos['nombre'] = $request->input('nombre');
        $datos['desde'] = $request->input('desde');
        $datos['hasta'] = $request->input('hasta');
        return view('horasextra.create', compact('datos'));
    }

    /**
    * Reporte de Horas extras por persona
    *
    * @return \Illuminate\Http\Response        
    */
    public function reporte(Request $request)
    {
        $nombreDias = array();
        $años = $this->años;
        $meses = $this->meses;
        $accProcesos = new AccesoProcesos;
        $registroshex = '';
        $registros = array();
        $nroRegistros = 0;
        $datos = array();
        $tipo_emp = '';
    
         if($request->has('cedula')){      
            $datos = $request->only('cedula','nombre','desde','hasta','año');
            $infoPersonal = \App\Models\Personal::where('cedula',$datos['cedula'])
                                                    ->with(['tipodeemp','horarios'])
                                                    ->first();
            
            $datos['nombre'] = $infoPersonal->nombre;
            $datos['mail'] = $infoPersonal->mail;
            $tipo_emp = $infoPersonal->tipodeemp->cod;
    
            $totalhextra = array();
            $fecProcesos = new FechasProcesos();              

            $registroshex = \App\Models\Horasextra::where('cedula',$request->cedula)
                                                    ->where('estado',TRUE)
                                                    ->where('pagadomes','>=',$datos['desde'])
                                                    ->where('pagadomes', '<=',$datos['hasta'])
                                                    ->where('pagadoano', '=', $datos['año'])
                                                    ->orderby('pagadoano')
                                                    ->orderby('pagadomes')
                                                    ->orderby('fecha')
                                                    ->get();
            $nroRegistros = count($registroshex);
            if($nroRegistros > 0){
                foreach($registroshex as $reg){
                    $nombreDias[$reg->fecha] = substr($fecProcesos->setFechaDia($reg->fecha),0,2);                
                    $registros[$reg->pagadoano][$reg->pagadomes][$reg->id] = $reg;
                }
                
                foreach($registros as $tmpano=>$tmp) {
                    foreach($tmp  as $tmpmes=>$listado){
                        $totalhextra[$tmpano][$tmpmes] = $accProcesos->sumarHorasOlympo($listado);
                    }
                } 
            }

             if($tipo_emp == '01' || $tipo_emp == '03' || $tipo_emp == '04' || $tipo_emp == '17'){
                 $col = $this->tipos_col["01"];
             }else{
                 $col = $this->tipos_col["02"];
             }
            
         }else{
             $datos['desde'] = date('m');
             $datos['hasta'] = date('m');
             $datos['año'] = date('Y');
             $datos['mail']='';
         }

            
        return view('horasextra.reporte', compact('años','meses','datos','infoPersonal','registros','nroRegistros',
                                                    'tipo_emp','col', 'totalhextra','nombreDias'));
    }

    public function reporteMes(Request $request)    
    {
        $registros = [];
        $accProcesos = new AccesoProcesos;
        $años = $this->años;
        $meses = $this->meses;
        $nroRegistros = 0;
        $datos = array();
        $nombres = array();
        $tipos = array();
        $encabezados_emp = array();        

        
        if($request->has('mes')){
            $datos = $request->only('mes','año');
 //dd($datos);           
            $totalhextra = array();
            $fecProcesos = new FechasProcesos();

            ($request->has('tipoSeleccionada'))? $tipoemp = $request->input('tipoSeleccionada'):$tipoemp = 0;
            ($tipoemp == 0)? $qtipoemp = '>':$qtipoemp='=';
            $datos['tipoSeleccionada'] = $tipoemp;

            ($request->has('tipoHorario'))?$horario = $request->input('tipoHorario'):$horario = 0;
            ($horario == 0)? $qhorario = '>':$qhorario = '=';
            $datos['tipoHorario'] = $horario;

            $listaPersonal = \App\Models\Personal::where('estado','>','0')
                                ->where('tipo_emp',$qtipoemp, $tipoemp)
                                ->where('horario', $qhorario, $horario)
                                ->where('horario','!=','5')
                                ->orderBy('nombre')
                                ->get();
            $cedulas = array();
            $correos = array();
            $listacorreos = "";
            foreach($listaPersonal as $persona){
                array_push($cedulas,$persona->cedula);
                $tipos[$persona->nombre] = $persona->tipodeemp->cod;
                if(strlen($persona->mail) > 2){
                    $correos[$persona->nombre] = $persona->mail;
                }    
            }

            $registroshex = \App\Models\Horasextra::whereIn('cedula',$cedulas)
                                                    ->where('estado',TRUE)
                                                    ->where('pagadoano','=',$datos['año'])
                                                    ->where('pagadomes','=',$datos['mes'])
 //                                                   ->where('tipo_emp',$qtipoemp, $tipoemp)
 //                                                   ->where('horario', $qhorario, $horario)
                                                    ->orderby('cedula')
                                                    ->orderby('fecha')
                                                    ->with('persona')
                                                    ->get();

            $nroRegistros = count($registroshex);
            if($nroRegistros > 0){
                foreach($registroshex as $reg){
                    $nombreDias[$reg->fecha] = substr($fecProcesos->setFechaDia($reg->fecha),0,2);  
                    $nombre = $reg->persona['nombre'];
                    $registros[$nombre][$reg->id] = $reg;

                    if(isset($tipos[$nombre]) && (strcmp($tipos[$nombre],"01")==0 || strcmp($tipos[$nombre],"03")==0 || strcmp($tipos[$nombre],"04")==0 || strcmp($tipos[$nombre],"17")==0) ){
                        $encabezados_emp[$nombre] = $this->tipos_col["01"];

                    }else{
                        $encabezados_emp[$nombre] = $this->tipos_col["02"];
                    }
                }

                ksort($registros);
                
                foreach($registros as $cedula=>$tmp){
                    $tt = $accProcesos->sumarHorasExtra($tmp);
                    $totalhextra[$cedula]['normal'] = $accProcesos->sumarHorasExtra($tmp);
//dd($tt);
                    $totalhextra[$cedula]['olympo'] = $accProcesos->totalHorasExtaOlympo($tt);
                }
            }    

        }else{
           $datos['tipoHorario']=0;
           $datos['tipoSeleccionada']=0;
        }
//dd($encabezados_emp);
        $ltiposemp =  \App\Models\Tipoemp::orderBy('nombre')->get();
        $lhorarios =  \App\Models\Horarios::orderBy('nombre')->get();
//dd($registros);
        return view('horasextra.reporteMes', compact('años','meses','datos','registros', 'correos', 
            'nroRegistros', 'totalhextra','nombreDias','ltiposemp','lhorarios', 'encabezados_emp'));
    }

    public function reporteTh(Request $request){

        $registros = [];
        $accProcesos = new AccesoProcesos;
        $años = $this->años;
        $meses = $this->meses;
        $nroRegistros = 0;
        $datos = array();
        $nombres = array();
        $tipos = array();
        $encabezados_emp = array();
        $necabezados = array();
        $totalOlympo = '';
        $totalHoras = '';
        $datos = array('mes'=>date('m'),'año'=>date('Y'));

        $ltiposemp =  \App\Models\Tipoemp::orderBy('nombre')->get();
        $lhorarios =  \App\Models\Horarios::orderBy('nombre')->get();

        if($request->has('mes')){
            $datos = $request->only('mes','año');
            //dd($datos);
            $totalhextra = array();
            $fecProcesos = new FechasProcesos();
            $dhorario = array();
            $totalOlympo = '00:00:00';
            $totalHoras = '00:00:00';

            ($request->has('tipoSeleccionada'))? $tipoemp = $request->input('tipoSeleccionada'):$tipoemp = 0;
            ($tipoemp == 0)? $qtipoemp = '>':$qtipoemp='=';
            $datos['tipoSeleccionada'] = $tipoemp;
            $datos['ntipoSeleccionada'] = 'TODO EL PERSONAL';
            foreach($ltiposemp as $lt){
                if($tipoemp == $lt->codigo)
                    $datos['ntipoSeleccionada'] = $lt->nombre;
            }

            $listaPersonal = \App\Models\Personal::where('estado','>','0')
                ->where('tipo_emp',$qtipoemp, $tipoemp)
                ->where('horario','!=','5')
                ->orderBy('nombre')
                ->get();
            $cedulas = array();
            $correos = array();
            $listacorreos = "";
            foreach($listaPersonal as $persona){
                array_push($cedulas,$persona->cedula);
                $tipos[$persona->nombre] = $persona->tipodeemp->cod;
                if(strlen($persona->mail) > 2){
                    $correos[$persona->nombre] = $persona->mail;
                }
            }

            $registroshex = \App\Models\Horasextra::whereIn('cedula',$cedulas)
                ->where('estado',TRUE)
                ->where('pagadoano','=',$datos['año'])
                ->where('pagadomes','=',$datos['mes'])
                ->orderby('cedula')
                ->orderby('fecha')
                ->with('persona')
                ->get();

            $nroRegistros = count($registroshex);
            if($nroRegistros > 0){
                foreach($registroshex as $reg){
                    $nombreDias[$reg->fecha] = substr($fecProcesos->setFechaDia($reg->fecha),0,2);
                    $nombre = $reg->persona['nombre'];
                    $registros[$nombre][$reg->id] = $reg;
                    if(isset($tipos[$nombre]) && (strcmp($tipos[$nombre],"01")==0 || strcmp($tipos[$nombre],"03")==0 || strcmp($tipos[$nombre],"04")==0 || strcmp($tipos[$nombre],"17")==0) ){
                        $encabezados_emp[$nombre] = $this->tipos_col["01"];

                    }else{
                        $encabezados_emp[$nombre] = $this->tipos_col["02"];
                    }

                    if(strcmp('01',substr($reg->fecha,-2))==0){
                        if(strlen($reg->ingreso)<1 && strlen($reg->salida)<1){
                            $nmes = substr($reg->fecha,5,2);
                            $nmes = $this->meses[$nmes];
                            $registros[$nombre][$reg->id]['todo'] = 'MES DE '.strtoupper($nmes); 
                        }
                    }
                    $registros[$nombre][$reg->id]['hor'] = $accProcesos->textoIngresoSalida($reg->horario);
  

                }

                if($tipoemp == '01' || $tipoemp == '02' || $tipoemp == '04' || $tipoemp == '17'){
                    $encabezados = $this->tipos_col["01"];
                }else{
                    $encabezados = $this->tipos_col["02"];
                }

                ksort($registros);

                foreach($registros as $cedula=>$tmp){
                    $tt = $accProcesos->sumarHorasExtra($tmp);
                    $totalhextra[$cedula]['normal'] = $accProcesos->sumarHorasExtra($tmp);
                    $totalHoras = $fecProcesos->sumarHorasExtra($totalHoras,$totalhextra[$cedula]['normal']['total']);
                    //dd($totalhextra[$cedula]['normal']['total']);
                    $ttOlympo = $accProcesos->totalHorasExtaOlympo($tt);
                    $totalhextra[$cedula]['olympo'] = $ttOlympo;
                    $ttOlympo = $ttOlympo['total'].':00';
                    $totalOlympo = $fecProcesos->sumarHorasOlympo($totalOlympo,$ttOlympo);

                }
                
            }

        }else{
            $datos['tipoHorario']=0;
            $datos['tipoSeleccionada']=0;
        }

        $totalOlympo = substr($totalOlympo,0,-3);
        $totalHoras = substr($totalHoras,0,-3);
//dd($registros['ACOSTA ACOSTA KLEVER ESTEVAN']);
        return view('horasextra.reporteTh', compact('años','meses','datos','registros', 'correos', 'tipoemp', 'dhorario', 'totalOlympo','totalHoras',
            'nroRegistros', 'totalhextra','nombreDias','ltiposemp','lhorarios', 'encabezados_emp','encabezados'));
    }
    
    /**
     * Registro de la devolución de horas extra.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function devolucion(Request $request)
    {
        $años = $this->años;
        $meses = $this->meses;
        $datos = array('mes'=>date('m'),'año'=>date('Y'));
        $tipo_emp = 0;
        return view('horasextra.devolucion', compact('datos','años','meses','tipo_emp'));
    }
    public function devolucionRegistro(Request $request)
    {
        $años = $this->años;
        $meses = $this->meses;
        $datos = array('mes'=>date('m'),'año'=>date('Y'));
        
        if($request->has('cedula')){      
            $datos = $request->only('cedula','nombre','mes','año'); 
            $infoPersonal = \App\Models\Personal::where('cedula',$datos['cedula'])
                                                    ->with(['tipodeemp','horarios','recesos'])
                                                    ->first();
  //          $infoPersonal = $infoPersonal[0];
            $datos['nombre'] = $infoPersonal->nombre;
            $tipo_emp = $infoPersonal->tipodeemp->cod;
            $horario = $infoPersonal->horarios;
            $receso = $infoPersonal->recesos;
        }    
        return view('horasextra.devolucionRegistro', compact('años','meses','datos'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $id = $request->input('id');
        $estado = FALSE;
        $borradopor = ''.$this->user->name;
        \App\Models\Horasextra::where('id','=',$id)
                                    ->update(['estado'=>$estado,'borradopor'=>$borradopor]);
                                    
        $datos = $request->only('cedula','nombre','mes','año');
        
        return redirect()->route('horasextra.ingreso',$datos);
        
        //return redirect(route('personal.index'));
    }
}
