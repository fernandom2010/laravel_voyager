<?php

namespace App\Http\Controllers\Rrhh;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Procesos\FechasProcesos;
use App\Procesos\AccesoProcesos;
use App\Models\Horasextra;
use App\Models\Hospedajeproveedor;

class HospedajeproveedoresController extends Controller
{

    private $años;
    private $meses;
    private $tipos_col;
    protected $user;

    function __construct()
    {
        $fecProcesos = new FechasProcesos();
        $this->meses = $fecProcesos->setMeses();
        $this->años = $fecProcesos->setAños();
        $this->user = Auth::user();
        $this->tipos_col = array(
            "02"=>array(
                'suple' => 2,
                'extra' => 1,
                25=> 'Nocturna <br/>25%',
                50=> 'Normal <br/>50%',
                '25l'=> 'Nocturna 25%',
                '50l'=> 'Normal 50%',
            ),
            "01"=>array(
                'suple' => 1,
                'extra' => 2,
                25 => '25%',
                50 => '60%',
                '25l' => '25%',
                '50l' => '60%'
            )
        );
    }

    public function index(Request $request)
    {   
        $datos = array('id'=>0);
        $frm_destino = 'index';
        $formulario = 0;
        if($request->has('Guardar')){
            $prov = new Hospedajeproveedor;
            $prov->nombre = $request->nombre;
            $prov->comercial = $request->comercial;
            $prov->campamento_id = $request->campamento;
            $prov->precio = $request->precio;
            $prov->save();
        }


        $lista = \App\Models\Hospedajeproveedor::where('activo','true')
                                                    ->with(['campamento'])
                                                    ->orderby('nombre')
                                                    ->get(); 

        $campamentos = \App\Models\Hospedajecampamento::where('activo','true')
                                                        ->orderby('nombre')
                                                        ->get(); 
              
        return view('hospedajeproveedores.index', compact('lista','formulario','datos','frm_destino','campamentos'));

    }

    public function modificar(Request $request, $id=0){
        $formulario = 1;
        $frm_destino = 'modificar';
        $datos['id'] = 0 ;

        if($request->has('id') && $request->input('id')>0 ){
            $dat = $request->only(['id','nombre','precio','comercial','campamento_id']);           
            \App\Models\Hospedajeproveedor::where('id',$request->input('id'))
                                                ->update($dat);
            $formulario = 0;            
        }

        if($formulario > 0){
            $datos = \App\Models\Hospedajeproveedor::where('id',$id)
                                                ->first()->toArray();    
        }        
       
        $lista = \App\Models\Hospedajeproveedor::where('activo','true')
                                                    ->orderby('nombre')
                                                    ->get();  
        $campamentos = \App\Models\Hospedajecampamento::where('activo','true')
                                                        ->orderby('nombre')
                                                        ->get(); 
              
        return view('hospedajeproveedores.index', compact('lista','formulario','datos','frm_destino','campamentos'));                                                

    }

    public function eliminar($id)
    {
        
        $borradopor = "'".$this->user->name."'";
        \App\Models\Hospedajeproveedor::where('id','=',$id)
                                            ->update(['activo'=>'false','borradopor'=>$borradopor]);

        return redirect()->route('proveedores.index');
    }

    public function frmreporte()
    {
        
        $proveedores = \App\Models\Hospedajeproveedor::where('activo','true')
                                                        ->orderby('nombre')
                                                        ->get();

        return view('hospedajeproveedores.frmreporte', compact('proveedores'));
    }

    public function reporte(Request $request)
    {
        $desde = $request->input('desde');
        $hasta = $request->input('hasta');
        $firmas = $request->only(['preparado','preparadocargo','autorizado','autorizadocargo']);
        $proveedor_id = $request->input('proveedor');
        $hoy = date('Y-m-d');
        $perFechas = new FechasProcesos();        
        $totalDias = 0;
        $totalValor= 0;
        $meses = array();        
        $ctotal = 0;
        $rpdf = 0;

        if($request->has('rpdf')){
            $rpdf = $request->input('rpdf');
        }

        $proveedor = \App\Models\Hospedajeproveedor::where('id',$proveedor_id)                                         ->with(['campamento'])               
                                                        ->first();
                                                        
        $hospedaje = \App\Models\Hospedaje::where('proveedor_id',$proveedor_id)
                                            ->where('activo','true')
                                            ->where('fecha','>=',$desde)
                                            ->where('fecha','<=',$hasta)
                                            ->orderby('fecha')
                                            ->with(['persona'])
                                            ->get();
        foreach($hospedaje as $hos){
            $fecha = explode("-",$hos->fecha);
            $mes = $fecha[1];
            $meses[$mes] = $mes;            
            $tabla[$hos->cedula]['nombre'] = $hos->persona->nombre;
            $tabla[$hos->cedula]['meses'][$mes][$hos->fecha] = $hos->persona->nombre; 
        }

        foreach($meses as $m){
            $meses[$m] = $perFechas-> setNombreMes($m);
        }
        $ctotal = 2 + count($meses)*2;
       
        foreach($tabla as $ced=>$tab){
            $tt = 0;
            foreach($tab['meses'] as $m=>$d){
                $tabla[$ced]['meses'][$m]['t'] = count($d);
                $tt = $tt + count($d);
            }
            $tabla[$ced]['t'] = $tt;
            $valor = $tt * $proveedor->precio;
            $valor = sprintf("%01.2f",$valor);
            $tabla[$ced]['v'] = $valor;
            $totalDias = $totalDias + $tt;
            $totalValor= $totalValor+ $valor;
        }

        if($rpdf > 0){
            $view = \View::make('hospedajeproveedores.reportepdf',compact('proveedor','desde','hasta','hospedaje','meses','tabla','hoy','totalDias','totalValor','ctotal','firmas'))->render();
            $pdf = \App::make('dompdf.wrapper');
            $pdf->loadHTML($view)->setPaper('a4','landscape');
            return $pdf->stream('invoice');    
        }        

        return view('hospedajeproveedores.reporte', compact('proveedor','desde','hasta','hospedaje','meses','tabla','hoy','totalDias','totalValor','ctotal','firmas'));
    }




}