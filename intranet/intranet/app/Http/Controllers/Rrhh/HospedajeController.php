<?php

namespace App\Http\Controllers\Rrhh;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Procesos\FechasProcesos;
use App\Procesos\AccesoProcesos;
use App\Models\Hospedaje;
use App\Models\Horasextra;

class HospedajeController extends Controller
{

    private $años;
    private $meses;
    private $tipos_col;
    protected $user;

    function __construct()
    {
        $fecProcesos = new FechasProcesos();
        $this->meses = $fecProcesos->setMeses();
        $this->años = $fecProcesos->setAños();
        $this->user = Auth::user();
        $this->tipos_col = array(
            "02"=>array(
                'suple' => 2,
                'extra' => 1,
                25=> 'Nocturna <br/>25%',
                50=> 'Normal <br/>50%',
                '25l'=> 'Nocturna 25%',
                '50l'=> 'Normal 50%',
            ),
            "01"=>array(
                'suple' => 1,
                'extra' => 2,
                25 => '25%',
                50 => '60%',
                '25l' => '25%',
                '50l' => '60%'
            )
        );
    }

    public function index(Request $request)
    {
        $años = $this->años;
        $meses = $this->meses;
        $datos = array('mes'=>date('m'),'año'=>date('Y'));
        //$datos = array('mes'=>'08','año'=>date('Y'));

        $tipo_emp = 0;

        return view('hospedaje.index', compact('datos','años','meses','tipo_emp'));

    }

    public function ingreso(Request $request)
    {
        $años = $this->años;
        $meses = $this->meses;
        $datos = array();
        $accProcesos = new AccesoProcesos;
        $fecProcesos = new FechasProcesos();
        $tipo_emp = 0;
        $horario = array();
        $receso = array();
        $lista = array();
        $nombreDias = array();
        $impHorario = array('ing_m'=>'','rec_sal'=>'','rec_ing'=>'','sal_t'=>'');
        $txHorario = '';

        $proveedores = \App\Models\Hospedajeproveedor::where('activo','true')
                                                        ->orderby('nombre')
                                                        ->get();               

        if($request->has('cedula')){      
            $datos = $request->only('cedula','nombre','mes','año'); 
            $mes = $datos['mes'];
            $año = $datos['año'];
            $infoPersonal = \App\Models\Personal::where('cedula',$datos['cedula'])
                                                    ->with(['tipodeemp','horarios','recesos'])
                                                    ->first();
  //          $infoPersonal = $infoPersonal[0];
            $datos['nombre'] = $infoPersonal->nombre;
            $tipo_emp = $infoPersonal->tipodeemp->cod;
            $horario = $infoPersonal->horarios;
            $receso = $infoPersonal->recesos;

            $txHorario = substr($horario->ing_m,0,5).'$';
            $impHorario['ing_m'] = substr($horario->ing_m,0,5);
            if(is_object($receso)){
                $txHorario = $txHorario.substr($receso->salida,0,5).'$';
                $impHorario['rec_sal'] = substr($receso->salida,0,5);
                $txHorario = $txHorario.substr($receso->ingreso,0,5).'$';
                $impHorario['rec_ing'] = substr($receso->ingreso,0,5);
            }else{
                $txHorario = $txHorario.' $ $';
            }
            $txHorario = $txHorario.substr($horario->sal_t,0,5);
            $impHorario['sal_t'] = substr($horario->sal_t,0,5);
                                                    
        }

        /***
        *   Generar reporte de alimentacion
        */

        $registros = array();
        $desde = date('Y-m-d',mktime(0,0,0,$datos['mes'],1,$datos['año']));
        $hasta = date('Y-m-d',mktime(0,0,0,$datos['mes']+1,0,$datos['año']));

        if($request->has('cedula')){
            $registroshos = \App\Models\Alimentacion::where('cedula',$request->cedula)
                                                ->where('fecha','>=',$desde)
                                                ->where('fecha', '<=',$hasta)
                                                ->orderby('fecha')                 
                                                ->get();
            foreach ($registroshos as $reg) {
                $nombreDias[$reg->fecha] = substr($fecProcesos->setFechaDia($reg->fecha),0,2);
                $registros[$reg->fecha] = $reg;
            }
        }

        $lis = \App\Models\Hospedaje::where('activo',true)
                                        ->where('cedula',$request->cedula)
                                        ->where('fecha','>=',$desde)
                                        ->where('fecha', '<=',$hasta)
                                        ->orderby('fecha')
                                        ->with(['proveedor'])
                                        ->get();
           
        foreach($lis as $l){
            $lista[$l->fecha] = array('id'=>$l->id,'fecha'=>$l->fecha,'cedula'=>$l->cedula,
                                      'lugar'=>$l->lugar,'trabajo'=>$l->trabajo,
                                      'ingreso'=>$l->ingreso,'salida'=>$l->salida,'tipo'=>$l->tipo,
                                      'proveedor'=>$l->proveedor->nombre);
        }
        
        $dia = $desde;
        while(strcmp($dia,$hasta)!=0){        
            if(!isset($registros[$dia])){
                $registros[$dia] = array('fecha'=>$dia);
                $nombreDias[$dia] = substr($fecProcesos->setFechaDia($dia),0,2);
            }
            $ddia = explode("-",$dia);
            $dia = date('Y-m-d',mktime(0,0,0,$ddia[1],$ddia[2]+1,$ddia[0]));
        }
        
        ksort($registros);
//dd($lista);
        return view('hospedaje.ingreso', compact('años','meses','datos','tipo_emp','txHorario','lista','mes','año',
                                            'impHorario','registros','nombreDias', 'proveedores'));
    }

    public function save(Request $request)
    {
        $cedula = $request->cedula;
        $año = $request->año;
        $mes = $request->mes;

        if($request->has('Guardar')){
            $hos = new Hospedaje;
            $hos->fecha = $request->fecha;
            $hos->cedula = $request->cedula;
            $hos->lugar = $request->lugar;
            $hos->trabajo = $request->trabajo;
            $hos->ingreso = $request->ingreso;
            $hos->salida = $request->salida;
            $hos->tipo = $request->tipo;
            $hos->proveedor_id = $request->proveedor_id;            

            $hos->save();
            
        }

        return redirect()->route('hospedaje.ingreso',['cedula'=>$cedula,'año'=>$año,'mes'=>$mes]); 
    }

    public function eliminar($id, $cedula, $año, $mes )
    {
        $borradopor = "'".$this->user->name."'";
        \App\Models\Hospedaje::where('id','=',$id)
                                            ->update(['activo'=>'false','borradopor'=>$borradopor]);

        return redirect()->route('hospedaje.ingreso',['cedula'=>$cedula,'año'=>$año,'mes'=>$mes]); 
    }

}