<?php

namespace App\Http\Controllers\Rrhh;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class RecesoController extends Controller
{
    public function index(Request $request)
    {
        ($request->has('seccionSeleccionada'))? $seccion = $request->input('seccionSeleccionada'):$seccion = 0;
        ($seccion == 0)? $qseccion = '>':$qseccion='=';

        ($request->has('tipoHorario'))?$horario = $request->input('tipoHorario'):$horario = 0;
        ($horario == 0)? $qhorario = '>':$qhorario = '=';

        $listaPersonal = \App\Models\Personal::where('estado','>','0')
            ->where('seccion',$qseccion,$seccion)
            ->where('horario', $qhorario, $horario)
            ->where('horario','!=','5')
  //          ->where('activo','=','1')
            ->orderBy('nombre')
            ->get();
//dd($listaPersonal);
        $lsecciones = \App\Models\Secciones::orderBy('nombre')->get();
        $datosSeccion="";
        if($seccion >0)
            $datosSeccion = \App\Models\Secciones::where('id',$seccion)->first();

        $lhorarios =  \App\Models\Horarios::orderBy('nombre')->get();
        $datosHorario="";
        if($horario>0)
            $datosHorario = \App\Models\Horarios::where('id',$horario)->first();

        $lrecesos = \App\Models\Recesos::where('activo','1')->orderBy('id', 'DESC')->get();

        return view('recesos.index', compact('listaPersonal', 'lsecciones','lhorarios',
            'lrecesos', 'seccion','horario', 'datosSeccion','datosHorario'));
    }

    public function actualizar(Request $request, $cedula, $receso)
    {
        \App\Models\Personal::where('cedula',$cedula)
            ->update(['receso'=>$receso]);

        $datos = \App\Models\Personal::where('cedula',$cedula)->get();

        return view('recesos.actualizar', compact('cedula','receso','datos'));
    }

}
