<?php

namespace App\Http\Controllers\Rrhh;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Procesos\FechasProcesos;
use App\Procesos\AccesoProcesos;
use App\Models\Horasextra;
use App\Models\Hospedajecampamento;

class HospedajecampamentosController extends Controller
{

    private $años;
    private $meses;
    private $tipos_col;
    protected $user;

    function __construct()
    {
        $fecProcesos = new FechasProcesos();
        $this->meses = $fecProcesos->setMeses();
        $this->años = $fecProcesos->setAños();
        $this->user = Auth::user();
        $this->tipos_col = array(
            "02"=>array(
                'suple' => 2,
                'extra' => 1,
                25=> 'Nocturna <br/>25%',
                50=> 'Normal <br/>50%',
                '25l'=> 'Nocturna 25%',
                '50l'=> 'Normal 50%',
            ),
            "01"=>array(
                'suple' => 1,
                'extra' => 2,
                25 => '25%',
                50 => '60%',
                '25l' => '25%',
                '50l' => '60%'
            )
        );
    }

    public function index(Request $request)
    {
        $datos = array('id'=>0);
        $frm_destino = 'index';
        $formulario = 0;
        if($request->has('Guardar')){
            $hos = new Hospedajecampamento;
            $hos->nombre = $request->nombre;
            $hos->save();
        }
        $lista = \App\Models\Hospedajecampamento::where('activo','true')
                                                    ->orderby('nombre')
                                                        ->get();     
          
        return view('hospedajecampamentos.index', compact('lista','formulario','datos','frm_destino'));
    }

    public function modificar(Request $request, $id=0){
        $formulario = 1;
        $frm_destino = 'modificar';
        $datos['id'] = 0;

        if($request->has('id') && $request->input('id') >0 ){
            $dat = $request->only(['id','nombre']);
            \App\Models\Hospedajecampamento::where('id',$request->input('id'))
                                                ->update($dat);
            $formulario = 0;
        }
        if($formulario >0){
            $datos = \App\Models\Hospedajecampamento::where('id',$id)
                                                ->first()->toArray();
        }
        $lista = \App\Models\Hospedajecampamento::where('activo','true')
                                                    ->orderby('nombre')
                                                        ->get();              
        return view('hospedajecampamentos.index', compact('lista','formulario','datos','frm_destino'));

    }

    public function eliminar($id) {
        $borradopor = "'".$this->user->name."'";
        \App\Models\Hospedajecampamento::where('id','=',$id)
                                            ->update(['activo'=>'false','borradopor'=>$borradopor]);
        return redirect()->route('campamentos.index');
    }

    public function frmreporte()
    {
        
        $campamentos = \App\Models\Hospedajecampamento::where('activo','true')
                                                        ->orderby('nombre')
                                                        ->get();

        return view('hospedajecampamentos.frmreporte', compact('campamentos'));
    }

    public function reporte(Request $request)
    {
        $desde = $request->input('desde');
        $hasta = $request->input('hasta');
        $campamento_id = $request->input('campamento');

        $campamento = \App\Models\Hospedajecampamento::where('id',$campamento_id)                                                        
                                                        ->first();
        $hospedaje = \App\Models\Hospedaje::where('campamento_id',$campamento_id)
                                            ->where('activo','true')
                                            ->where('fecha','>=',$desde)
                                            ->where('fecha','<=',$hasta)
                                            ->get();
//dd($hospedaje);
        return view('hospedajecampamentos.reporte', compact('campamento','desde','hasta','hospedaje'));

    }



}