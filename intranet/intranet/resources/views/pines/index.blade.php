@extends('layouts.appPines')
@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-10 col-md-offset-1">
      <div class="panel panel-default">
          <div class="panel-heading">
            PINES
          </div>
          <div class="panel-body">
            <table class="table">
              <thead>
                <tr>
                  <th>Nro</th>
                  <th>Cedula</th>
                  <th>Nombres</th>
                  <th>Pines</th>
                  <th>Estado</th>
                  <th>Actualizar</th>
                </tr>
              </thead>
              <tbody>
                @foreach($listaPersonal as $cedula=>$persona)
                  <tr>
                    <td>{{$persona['nro']}}</td>
                    <td>{{$cedula}}</td>
                    <td>{{$persona['nombre']}}</td>
                    <td>
                      <a onclick="javascript:alert('{{$persona['pin']}}');">
                              <i class="fa fa-phone" aria-hidden="true"></i>
                            </a>
                    </td>
                    <td>
                      <?php
                        ($persona['activo']==1)?$icono="<i class='fa fa-check-circle-o fa-2x' aria-hidden='true' ></i>":$icono="<i class='fa fa-circle-thin' aria-hidden='true'></i>";                        
                      ?>
                      <a href="{{route('pines.habilitar',[$cedula])}}" >@php echo $icono @endphp</a>
                    </td>
                    <td>
                      <a href="{{route('pines.actualizar',[$cedula])}}" ><i class="fa fa-refresh" aria-hidden="true"></i></a>
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
      </div>
    </div>
  </div>
</div>
@endsection