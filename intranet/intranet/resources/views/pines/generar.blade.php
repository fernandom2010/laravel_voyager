@extends('layouts.appPines')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                  GENERAR PINES
                </div>
                <div class="panel-body">
                    <!--<div class="container">-->
                    <div class="form-group pull-right">
	                       <input type="text" id="buscado" class="search form-control" placeholder="Buscar?" onKeyUp="busquedaCertificados()">
                    </div>

                    <span class="counter pull-right"></span>
                    <div id="tabla">
            <table class="table">
              <thead>
                <tr>
                  <th>Cedula</th>
                  <th>Nombres</th>

                  <th>Pines</th>
                  <th></th>
                  <th></th>
                </tr>
              </thead>
              <tbody>

                  <?php
                  foreach($listaPersonal as $cedula=>$datos){
                    echo '<tr>'.
                          '<td>'.$cedula.'</td>'.
                          '<td>'.$datos['nombre'].'</td>'.
                          '<td>'.$datos['pin'].'</td>'.
                          '<td>
                            <a onclick="javascript:alert('."'".$datos['pin']."'".');">
                            <i class="fa fa-phone" aria-hidden="true"></i>
                            </a>
                          </td>'.
                        '</tr>';
                  }
                  ?>
              </tbody>
            </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>







@endsection
