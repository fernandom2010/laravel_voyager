    <nav class="navbar navbar-default navbar-inverse navbar-static-top">
        <div class="container">
		    <div class="navbar-header">
		        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
    			    <span class="sr-only">Toggle Navigation</span>
    			    <span class="icon-bar"></span>
    			    <span class="icon-bar"></span>
    			    <span class="icon-bar"></span>
			      </button>
            <a class="navbar-brand" href="{{ url('/') }}">
                PREFECTURA DE IMBABURA
            </a>
		    </div>
		    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

          <ul class="nav navbar-nav">
            <li><?php echo link_to_action( 'Rrhh\PersonalController@inicio',
                    $title = 'Personal',
                    $parameters = '',
                    $attributes = []); ?>
            </li>
            <li><?php echo link_to_action( 'Rrhh\PersonalController@certificados',
                    $title = 'Certificados',
                    $parameters = '',
                    $attributes = []); ?>
            </li>
            <li><?php echo link_to_action( 'Rrhh\PersonalController@llamado',
                    $title = 'Llamados',
                    $parameters = '',
                    $attributes = []); ?>
            </li>
            <li><?php echo link_to_action( 'Rrhh\ReportesController@index',
                    $title = 'Reportes',
                    $parameters = '',
                    $attributes = []); ?>
            </li>
            <li><?php echo link_to_action( 'Rrhh\RecesoController@index',
                    $title = 'Recesos',
                    $parameters = '',
                    $attributes = []); ?>
            </li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Actualizar <span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><?php echo link_to_action( 'Rrhh\ImportarController@personal',
                        $title = 'Personal',
                        $parameters = '',
                        $attributes = []); ?></li>
                <li><?php echo link_to_action( 'Rrhh\ImportarController@cargos',
                        $title = 'Cargos',
                        $parameters = '',
                        $attributes = []); ?></li>
                <li><?php echo link_to_action( 'Rrhh\ImportarController@titulos',
                        $title = 'Titulos',
                        $parameters = '',
                        $attributes = []); ?></li>
                <li><?php echo link_to_action( 'Rrhh\ImportarController@formacion',
                        $title = 'Formacion',
                        $parameters = '',
                        $attributes = []); ?></li>
                <li><?php echo link_to_action( 'Rrhh\ImportarController@especialidad',
                        $title = 'Especialidad',
                        $parameters = '',
                        $attributes = []); ?></li>        
                <li><?php echo link_to_action( 'Rrhh\ImportarController@capacitacion',
                        $title = 'Capacitacion',
                        $parameters = '',
                        $attributes = []); ?></li>
              </ul>
            </li>


          </ul>


          <ul class="nav navbar-nav navbar-right">
              <!-- Authentication Links -->
              @if (Auth::guest())
                  <li><a href="{{ url('/login') }}">Login</a></li>
                  <li><a href="{{ url('/register') }}">Register</a></li>
              @else
                  <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                          {{ Auth::user()->name }} <span class="caret"></span>
                      </a>

                      <ul class="dropdown-menu" role="menu">
                          <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                      </ul>
                  </li>
              @endif
          </ul>
			</div>
		</div>
	</nav>
