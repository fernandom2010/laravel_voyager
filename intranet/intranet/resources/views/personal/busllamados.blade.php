@extends('layouts.ajax')
@section('content')

            <table class="table">
              <thead>
                <tr>

                  <th>Nombres</th>
                  <th>Cedula</th>
                  <th>Cargo</th>
                  <th></th>
                  <th></th>
                </tr>
              </thead>
              <tbody>

                  <?php $i = 1; ?>
                  @foreach($listaPersonal as $persona)

                    <tr>
                        
                        <td>{{ $persona->nombre }}</td>
                        <td>{{ $persona->cedula }}</td>
                        <td>{{ $persona->cargos->nombre }}</td>
                        <?php
                        //$cargo  = $persona->cargos->nombre;
                        $cedula = $persona->cedula;
                        //if(strlen($cargo)>0){
                        ?>
  		                  <td>                            
                            <button type="button" class="btn btn-info btn-sm"
                                    data-toggle="modal" data-target="#myModal3"
                                    onclick="javascript:selectPersona3('{{$cedula}}','{{$persona->nombre}}')"
                                    > Uniforme </button>

                        </td>
                        <td>
                            <button type="button" class="btn btn-info btn-sm"
                                    data-toggle="modal" data-target="#myModal"
                                    onclick="javascript:selectPersona('{{$cedula}}','{{$persona->nombre}}')"
                                    > Horario </button>
                        </td> 
                        <td>
                            <button type="button" class="btn btn-info btn-sm"
                                    data-toggle="modal" data-target="#myModal1"
                                    onclick="javascript:selectPersona2('{{$cedula}}','{{$persona->nombre}}')"
                                    > Vacaciones </button>
                        </td>                        
                        <?php
                        //}
                        ?>
                    </tr>
                  @endforeach



              </tbody>
            </table>


@endsection
