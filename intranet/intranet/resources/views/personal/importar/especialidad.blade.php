@extends('layouts.appRrhh')
@section('content')

    <div class="section">
      <div class="container">
        <div class="row">
          <div class="col-md-2">
            <img src="/img/solo_sello.png" class="img-responsive" width="100px">
          </div>
          <div class="col-md-10">
            <h1> INFORMACIÓN DE ESPECIALIDADES ACTUALIZADA</h1>
          </div>
        </div>
      </div>
    </div>
<?php

?>

    <div class="section">
      <div class="container">
        <div class="row">
          <div class="col-md-12">

          <table class="table">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>NOMBRE</th>
                </tr>
              </thead>
              <tbody>

                  <?php $i = 1; ?>
                  @foreach($listado as $item)
                  <tr>
                      <td>{{ $item->id }}</td>
                      <td>{{ $item->descripcion }}</td>
                  </tr>
                  @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>



@endsection
