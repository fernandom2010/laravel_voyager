@extends('layouts.appRrhh')
@section('content')

    <div class="section">
      <div class="container">
        <div class="row">
          <div class="col-md-2">
            <img src="/img/solo_sello.png" class="img-responsive" width="100px">
          </div>
          <div class="col-md-10">
            <h1> DATOS DE CAPACITACION ACTUALIZADOS</h1>
          </div>
        </div>
      </div>
    </div>
<?php

?>

    <div class="section">
      <div class="container">
        <div class="row">
          <div class="col-md-12">

          <table class="table">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>CEDULA</th>
                  <th>DESCRIPCION</th>
                  <th>INSTITUCION</th>
                  <th>DURACION</th>
                  <th>DESDE</th>
                  <th>HASTA</th>
                </tr>
              </thead>
              <tbody>

                  @foreach($listado as $item)
                  <tr>
                      <td>{{ $item->id }}</td>
                      <td>{{ $item->cedula }}</td>
                      <td>{{ $item->descripcion }}</td>
                      <td>{{ $item->institucion }}</td>
                      <td>{{ $item->duracion }}</td>
                      <td>{{ $item->desde }}</td>
                      <td>{{ $item->hasta }}</td>
                  </tr>
                  @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>



@endsection
