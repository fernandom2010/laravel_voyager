@extends('layouts.appRrhh')
@section('content')

    <div class="section">
      <div class="container">
        <div class="row">
          <div class="col-md-2">
            <img src="/img/solo_sello.png" class="img-responsive" width="100px">
          </div>
          <div class="col-md-10">
            <h1> DATOS DE FORMACION ACTUALIZADOS</h1>
          </div>
        </div>
      </div>
    </div>
<?php

?>

    <div class="section">
      <div class="container">
        <div class="row">
          <div class="col-md-12">

          <table class="table">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>EMPLEADO</th>
                  <th>CODIGO</th>
                  <th>INSTRUCCION</th>
                  <th>NIVEL</th>
                  <th>INSTITUCION</th>
                  <th>TITULO</th>
                  <th>ESPECIALIDAD</th>
                  <th>FECHA</th>
                  <th>LUGAR</th>
                </tr>
              </thead>
              <tbody>

                  <?php $i = 1; ?>
                  @foreach($listado as $item)
                  <tr>
                      <td>{{ $item->id }}</td>
                      <td>{{ $item->nombres['nombre'] }}</td>
                      <td>{{ $item->codigo }}</td>
                      <td>{{ $item->instruccion }}</td>
                      <td>{{ $item->nivel }}</td>
                      <td>{{ $item->institucion }}</td>
                      <td>{{ $item->titulos['nombre'] }}</td>
                      <td>{{ $item->especialidades['descripcion'] }}</td>
                      <td>{{ $item->fecha }}</td>
                      <td>{{ $item->lugar }}</td>
                  </tr>
                  @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>



@endsection
