@extends('layouts.appRrhh')
@section('content')

    <div class="section">
      <div class="container">
        <div class="row">
          <div class="col-md-2">
            <img src="/img/solo_sello.png" class="img-responsive" width="100px">
          </div>
          <div class="col-md-10">
            <h1>ACTUALIZACION DE DATOS</h1>
          </div>
        </div>
      </div>
    </div>
<?php

?>

    <div class="section">
      <div class="container">
        <div class="row">
          <div class="col-md-12">

          <table class="table">
              <thead>
                <tr>
                  <th>TABLA</th>
                  <th>LINK</th>
                </tr>
              </thead>
              <tbody>
                  @foreach($links as $link)
                  <tr>
                      <td>{{ $link['title'] }}</td>
                      <td>{{ $link['link'] }}</td>
                      <td>
                      <?php
                        $atrib='target=blank';
                        echo link_to_action( $link['link'],
                                $title = $link['title'],
                                $parameters = '',
                                $attributes = [$atrib]);
                      ?>
                      </td>
                  </tr>
                  @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>



@endsection
