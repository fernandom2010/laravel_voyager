@extends('layouts.appRrhh')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        REPORTE

                    </div>

                    <div class="panel-body">

                        <!--<div class="container">-->
                        <center>
                            <a href="javascript:void(imprime())">
                                <i class='fa fa-print fa-3x' aria-hidden='true'></i>
                                <br>Imprimir
                            </a>
                        </center>
                        <hr>

                        <span class="counter pull-right"></span>
                        <div id="tabla">
                            <div id="imprimir">

                                <table align="center" class="encabezado"
                                       width="90%" border="0" bordercolor="with"><tr>
                                        <td align="left" width="20%" rowspan="2"><img  src="/img/logo.png" height="50px"></td>
                                        <th style="text-align: center;font-size: large;">PREFECTURA DE IMBABURA</th>
                                        <td align="right" width="20%" rowspan="2"><img  src="/img/escudo.png" width="50px"></td>
                                    </tr></table>


                                <table align="center" border="1" style="border-collapse:collapse">
                                    <thead>
                                    <tr>
                                        <th colspan="{{count($campos)}}"
                                            style="text-align: center;font-size: larger">
                                            {{$encabezado['titulo']}}
                                        </th>
                                    </tr>
                                    @if(isset($tipoemp['nombre']))
                                        <tr>
                                            <th colspan="2" >TIPO DE PERSONAL:</th>
                                            <th colspan="{{count($campos)}}" align="left">{{$tipoemp['nombre']}}</th>
                                        </tr>
                                    @endif
                                    @if(isset($departamento['nombre']))
                                    <tr>
                                        <th colspan="2" >DIRECCION:</th>
                                        <th colspan="{{count($campos)}}" align="left">{{strtoupper($departamento['nombre'])}}</th>
                                    </tr>
                                    @endif
                                    @if(isset($horario['nombre']))
                                    <tr>
                                        <th colspan="2" >HORARIO:</th>
                                        <th colspan="{{count($campos)}}" align="left">{{$horario['nombre']}}</th>
                                    </tr>
                                    @endif

                                    <tr>
                                        <?php
                                        foreach ($campos as $i => $n) {
                                            echo '<th>' . $n . '</th>';
                                        }
                                        ?>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    <?php
                                    foreach ($personal as $persona) {
                                        echo '<tr>';
                                        foreach ($campos as $i => $n) {
                                            echo '<td>' . $persona[$i] . '</td>';
                                        }
                                        echo '</tr>';
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>

                    <hr>
                    <center>
                        <a href="javascript:void(imprime())">
                            <i class='fa fa-print fa-3x' aria-hidden='true'></i>
                            <br>
                            Imprimir
                        </a>
                    </center>
                </div>
            </div>
        </div>
    </div>
@endsection
