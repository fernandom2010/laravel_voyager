@extends('layouts.appRrhh')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                  REPORTES
                </div>

                <div class="panel-body">
                  @if (count($errors) > 0)
                  <div class="alert alert-danger">
                      <ul>
                          @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                          @endforeach
                      </ul>
                  </div>
                  @endif
                  {!! Form::open(['action'=>'Rrhh\ReportesController@reporte', 'method'=>'post']) !!}
                      <input type="hidden" name="_token" value="{{csrf_token()}}">
                      <div class="form-group">
                          <label for="nombre">Titulo</label>
                          <input type="text" class="form-control" id="titulo" placeholder="Titulo del reporte"
                                  name="titulo" value="{{ old('titulo') }}">
                      </div>
                      <div class="form-group">
                          <label for="tipo_emp">Tipo</label>
                          <select id="tipo_emp" name="tipo_emp" class="form-control">
                              <option value="0">TODOS</option>
                              @foreach($tipodeemp as $tipo)
                              <option value="{{$tipo->codigo}}">{{$tipo->nombre}}</option>
                              @endforeach
                          </select>
                      </div>
                      <div class="form-group">
                          <label for="departamento">Departamento</label>
                          <select id="departamento" name="departamento" class="form-control">
                              <option value="0">TODOS</option>
                              @foreach($secciones as $seccion)
                              <option value="{{$seccion->id}}">{{$seccion->nombre}}</option>
                              @endforeach
                          </select>
                      </div>
                      <div class="form-group">
                          <label for="horario">Horario</label>
                          <select id="horario" name="horario" class="form-control">
                              <option value="0">TODOS</option>
                              @foreach($horarios as $horario)
                              <option value="{{$horario->id}}">{{$horario->nombre}}</option>
                              @endforeach
                          </select>
                      </div>

                      <div class="seccion">
                        <div class="container">
                           <?php
                              $j=0;
                              foreach($campos as $i => $c){

                                if($j == 0){
                                  echo '<div class="row">';
                                }
                                $j++;
                                echo  '<div class="col-md-3">'.
                                        '<div class="form-group">
                                          <label for="'.$i.'">'.$c.'</label>
                                          <input type="checkbox" id="'.$i.'" name="'.$i.'"
                                                 value="'.$c.'"/>
                                        </div>'.
                                      '</div>';
                                if($j == 3){
                                  echo '</div>';
                                  $j=0;
                                }
                              }

                           ?>
                        </div>
                      </div>
                      <button type="submit" class="btn btn-default">Submit</button>
                  {!! Form::close() !!}

                    <!--<div class="container">-->

                </div>
            </div>
        </div>
    </div>
</div>


@endsection
