@extends('layouts.ajax')
@section('content')

  <table class="table table-hover">
    <thead class="thead-inverse">
      <tr>
        <th>Cedula</th>
        <th>Nombres</th>
      </tr>
    </thead>
    <tbody>
    @foreach($listaPersonal as $persona)
      <tr onclick="datosPersonaCampo('{{$campo}}','{{$persona->cedula}}','{{$persona->nombre}}','{{$persona->cargos->nombre}}')">
        <td>{{$persona->cedula}}</td>
        <td>{{$persona->nombre}}</td>
        <td>{{$persona->cargos->nombre}}</td>
      </tr>
      @endforeach
    </tbody>
  </table>



@endsection
