@extends('layouts.ajax')
@section('content')

  <table class="table">
    <thead>
      <tr>
        <td rowspan="2">
          <img src="/img/personal/{{$persona->cedula}}.JPG" width="100px" /><br/>
          <!-- <a href=""><i class="fa fa-upload"></i>Subir foto</a><br/>
          <input type="file" name="image"  id="image"> -->
          {!! Form::open(['action'=>'Rrhh\PersonalController@subir', 
                            'method'=>'post', 
                            'enctype'=>"multipart/form-data",                            
                            'id'=>'subirFoto']) !!}  
                            
            <label for="file" class="custom-file-upload">                
                Seleccionar
            </label>          
            <input type="file" name="file" id="file" class="inputfile" />
            <input type="hidden" name="cedula" value="{{$persona->cedula}}" />
            <input type="hidden" name="id" value="{{$id}}" />
            <center>
                <button type="submit"><i class="fa fa-upload"></i></button>
            </center>    
          {!! Form::close() !!}     
        </td>
        <td colspan="6">
          <h3>{{$persona->nombre}}</h3>
          <a href="mailto:{{$persona->mail}}">{{$persona->mail}}</a>
        </td>
      </tr>
      <tr>
        <th>CEDULA</th>
        <td colspan="2"  class="text-primary">{{$persona->cedula}}</td>
        <th>CTA RELOJ</th>
        <td class="text-warning">{{$persona->cuenta}}</td>
        <td></td>
      </tr>
      <tr>
    </thead>
    <tbody>
      <tr>
        <th>DEPARTAMENTO</th><td>{{strtoupper($persona->secciones['nombre'])}}</td>
        <th>CARGO</th><td>{{$persona->cargos['nombre']}}</td>
        <th>SUELDO</th><td>{{sprintf("%01.2f",$persona->sueldo)}}</td>
      </tr>
      <tr>
      <!--
        <th>CTA CONTABLE</th><td>{{$persona->cuenta_contable}}</td>
      -->  
        <th>TIPO </th><td>{{$persona->tipodeemp['nombre']}}</td>
        <th>HORARIO</th><td>{{$persona->horarios['nombre']}}</td>
        <th>DENOMINACIÓN</th><td>{{$denominacion}}</td>
      </tr>
      <tr>
        <th>FECHA ING</th><td>{{$persona->ingreso}}</td>
        <th nowrap="nowrap">FECHA SAL</th><td>{{$fec_salida}}</td>
        <th>TIEMPO TRABA.</th><td>{{$tmp_trabajo}}</td>
      </tr>
      <tr>
      <tr>
          <th colspan="6"><center>DATOS PERSONALES</center></th>
      </tr>
      <tr>
          <th>NIVEL</th><td>{{$formacion['nivel']}}</td>
          <th>TITULO</th><td colspan="3" >{{$formacion['nombre']}}</td>
      </tr>
      <tr>
          <td colspan="2"></td>
          <th>ESPECIALIDAD</th><td colspan="3">{{$formacion['especialidad']}}</td>
      </tr>
      <tr>
          <th>FECHA NAC.</th><td>{{$persona->nacimiento}}</td>
          <th>EDAD</th><td>{{$nacimiento}}</td>
          <th>TIPO SANGRE</th><td>{{$persona->tipossangre['nombre']}}</td>

      </tr>
      <tr>
          <th>DIRECCION</th><td>{{$persona->direccion}}</td>
          <th>CIUDAD</th><td>{{$persona->ciudad}}</td>
          <th>TELEFONO</th><td>{{$persona->telefono}}</td>
      </tr>


    </tbody>
  </table>



@endsection
