@extends('layouts.appRrhh')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                  INGRESO DE PERSONAL
                </div>

                <div class="panel-body">
                  @if (count($errors) > 0)
                  <div class="alert alert-danger">
                      <ul>
                          @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                          @endforeach
                      </ul>
                  </div>
                  @endif
                  <form method="POST" action="{{route('personal.store')}}">
                      <input type="hidden" name="_token" value="{{csrf_token()}}">
                      <div class="form-group">
                          <label for="nombre">Nombre</label>
                          <input type="text" class="form-control" id="nombre" placeholder="Nombre" name="nombre" value="{{ old('nombre') }}">
                      </div>
                      <div class="form-group">
                          <label for="cedula">Cedula</label>
                          <input type="text" class="form-control" id="cedula" placeholder="Cedula" name="cedula" value="{{ old('cedula') }}">
                      </div>
                      <div class="form-group">
                          <label for="tipo_emp">Tipo</label>
                          <select id="tipo_emp" name="tipo_emp" class="form-control">
                              @foreach($tipodeemp as $tipo)
                              <option value="{{$tipo->id}}">{{$tipo->nombre}}</option>
                              @endforeach
                          </select>
                      </div>
                      <div class="form-group">
                          <label for="cargo">Cargo</label>
                          <select id="cargo" name="cargo" class="form-control">
                              @foreach($cargos as $cargo)
                              <option value="{{$cargo->id}}">{{$cargo->nombre}}</option>
                              @endforeach
                          </select>
                      </div>
                      <div class="form-group">
                          <label for="seccion">Seccion</label>
                          <select id="seccion" name="seccion" class="form-control">
                              @foreach($secciones as $seccion)
                              <option value="{{$seccion->id}}">{{$seccion->nombre}}</option>
                              @endforeach
                          </select>
                      </div>
                      <div class="form-group">
                          <label for="horario">Horario</label>
                          <select id="horario" name="horario" class="form-control">
                              @foreach($horarios as $horario)
                              <option value="{{$horario->id}}">{{$horario->nombre}}</option>
                              @endforeach
                          </select>
                      </div>
                      <button type="submit" class="btn btn-default">Submit</button>
                  </form>

                    <!--<div class="container">-->

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
