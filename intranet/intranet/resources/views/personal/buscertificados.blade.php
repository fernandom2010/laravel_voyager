@extends('layouts.ajax')
@section('content')


            <table class="table">
              <thead>
                <tr>                  
                  <th>Nombres</th>
                  <th>Cedula</th>
                  <th>Cuenta</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>

                  <?php $i = 1; ?>
                  @foreach($listaPersonal as $persona)
                  <tr>
                      
                      <td>{{ $persona->nombre }}</td>
                      <td>{{ $persona->cedula }}</td>
                      <td>{{ $persona->cuenta }}</td>
                      <?php $cedula = $persona->cedula; ?>
                      <td>
                            {!! Form::open(array('action' => array('PdfController@certificado1',$cedula), 'target'=>'blank', 'method'=>'get')) !!}
                            <button type="submit" class="btn btn-info btn-sm" > Certificado 1 </button>
                            {!! Form::close() !!}                      
                      </td>
                        <td>
                            <button type="button" class="btn btn-info btn-sm"
                                    data-toggle="modal" data-target="#myModal"
                                    onclick="javascript:selectPersona('{{$cedula}}','{{$persona->nombre}}')"
                                    > Certificado 2 </button>
                        </td>
                  </tr>
                  @endforeach



              </tbody>
            </table>



@endsection
