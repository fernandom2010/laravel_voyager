@extends('layouts.ajax')
@section('content')

  <table class="table table-hover">
    <thead class="thead-inverse">
      <tr>
        <th>#</th>
        <th></th>
        <th>Nombres</th>
        <th>Dirección</th>
	      <th>Tipo</th>
        <th>Horario</th>
      </tr>
    </thead>
    <tbody>
      <?php
    	$i = 1;
    	foreach($listaPersonal as $persona){
        echo '<tr>'.
          '<td class="texto10" >'.$i++.'</td>'.
          '<td><img src="/img/personal/'.$persona->cedula.'.JPG" width="30px" /></td>'.
          '<td><strong>'.$persona->nombre.'</strong><br><div class="texto10">'.$persona->cedula.'</div></td>'.

          '<td class="texto12">'.
              '<strong>'.$persona->secciones['nombre'].'</strong><br>'.
              '<div class="texto10">'.$persona->cargos['nombre'].'</div>'.
          '</td>'.
          '<td class="texto12">'.$persona->tipodeemp['nombre'].'</td>'.
          '<td class="texto12">'.
              '<strong>'.$persona->horarios['nombre'].'</strong><br>'.
              $persona->cuenta.'</td>'.
          '</td><td >'.
            '<button type="button"
                     class="btn btn-default"
                     data-toggle="modal" data-target="#myModal"
                     onclick="javascript:seleccion('."'".$persona->id."'".')" >Individual</button>'.
        '</tr>';
    	}
      ?>

    </tbody>
  </table>



@endsection
