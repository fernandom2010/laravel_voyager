@extends('layouts.appRrhh')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                  ACTUALIZAR PERSONAL

                </div>

                <div class="panel-body">
                    <!--<div class="container">-->
                    <table class="table">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Nombres</th>
                          <th>Cedula</th>
                          <th>Cuenta</th>

                        </tr>
                      </thead>
                      <tbody>

                      <?php
                        $i = 1;
                        foreach($lpersonal as $persona){
                            echo '<tr>'.
                              '<td>'.$i++.'</td>'.
                              '<td>'.$persona->nombre.'</td>'.
                              '<td>'.$persona->cedula.'</td>'.
                              '<td>'.$persona->cuenta.'</td>';

                            echo  '</tr>';
                        }
                      ?>

                      </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
