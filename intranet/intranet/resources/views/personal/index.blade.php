@extends('layouts.appRrhh')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        NOMINA DEL PERSONAL
                        <a href="{{route('personal.create')}}" class="pull-right">Add New</a>
                    </div>

                    <div class="panel-body">
                        <!--<div class="container">-->
                        <div class="form-group pull-right">
                            <table>
                                <tr>
                                    <td>
                                    {!! Form::open(['action'=>'Rrhh\PersonalController@inicio', 'method'=>'post', 'id'=>'formCesantes']) !!}
                                        Incluir Cesantes 
                                        <input type="checkbox" name="cesantes" id="cesantes" value="{{$cesantes}}" onclick="this.form.submit();" 
                                        @if(strcmp($cesantes,"500")==0)
                                            checked="cheked"
                                            @endif
                                        >
                                        @if($seccion > 0)
                                            <input type="hidden" name="seccionSeleccionada" value="{{$seccion}}" />
                                        @endif
                                        @if($tipoemp > 0)
                                            <input type="hidden" name="tipoSeleccionada" value="{{$tipoemp}}" />
                                        @endif
                                        @if($horario > 0)
                                            <input type="hidden" name="tipoHorario" value="{{$horario}}" />
                                        @endif
                                        
                                    {!! Form::close() !!}    
                                    </td>
                                    <td>&nbsp;&nbsp;|&nbsp;&nbsp;</td>
                                    <td>
                                        <a href="javascript:void(printSpecial('imprimir'))">
                                            <i class='fa fa-print fa-2x' aria-hidden='true'></i>
                                        </a>
                                    </td>
                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                    <td>
                                        <input type="text" id="buscado" class="search form-control"
                                               placeholder="Buscar?" onKeyUp="busquedaNomina()">
                                    </td>
                                </tr>
                            </table>

                        </div>

                        <span class="counter pull-right"></span>
                        <div id="imprimir">


                            <div id="tabla">
                                <table class="table table-hover">
                                    <thead class="thead-inverse">
                                    <tr>
                                        <th rowspan="2">#</th>
                                        <th rowspan="2"></th>
                                        <th rowspan="2">Nombres</th>
                                        <th>
                                            Dirección
                                        </th>
                                        <th nowrap>
                                            Tipo
                                        </th>
                                        <th nowrap>Horario

                                        </th>
                                        <th rowspan="2" nowrap> Filtrar
                                            <button type="button" class="btn btn-default" data-toggle="modal"
                                                    data-target="#myModal1">
                                                <i class="fa fa-search-plus" aria-hidden="true"></i>
                                            </button>
                                        </th>

                                    </tr>
                                    <tr>
                                        <th class="texto10">
                                            @if(is_object($datosSeccion))
                                                {{$datosSeccion->nombre}}
                                            @endif
                                        </th>
                                        <th class="texto10">
                                            @if(is_object($datosTipo))
                                                {{$datosTipo->nombre}}
                                            @endif
                                        </th>
                                        <th class="texto10">
                                            @if(is_object($datosHorario))
                                                {{$datosHorario->nombre}}
                                            @endif
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    <?php
                                    $i = 1;
                                    foreach ($listaPersonal as $persona) {
                                        echo '<tr>' .
                                            '<td class="texto10" >' . $i++ . '</td>' .
                                            '<td><img src="/img/personal/' . $persona->cedula . '.JPG" width="35px" />
                                            <a name="' . $persona->cedula . '"></a></td>' .
                                            '<td><strong>' . $persona->nombre .'</strong><br><div class="texto10">' . $persona->cedula . '</div></td>' .

                                            '<td class="texto12">' .
                                            '<strong>'.$persona->secciones['nombre'] . '</strong><br>' .
                                            '<div class="texto10">' . $persona->cargos['nombre'] . '</div>' .
                                            '</td>' .
                                            '<td class="texto12">' . $persona->tipodeemp['nombre'] . '</td>' .
                                            '<td class="texto12">' .
                                            '<strong>' . $persona->horarios['nombre'] . '</strong><br>' .
                                            $persona->cuenta . '</td>' .
                                            '</td><td >' .
                                            '<button type="button"
                                         class="btn btn-default"
                                         data-toggle="modal" data-target="#myModal"
                                         onclick="javascript:seleccion(' . "'" . $persona->id . "'" . ')" >Individual</button>' .
                                            '</tr>';
                                    }
                                    ?>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div id="myModal" class="modal fade">
        <div class="modal-dialog modal-lg">
            <div class="modal-content" id="individual">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" style="text-align: center">INFORMACION PERSONAL</h4>
                </div>
                <div class="modal-body">
                    <div id="idPersonal"></div>
                </div>
                <div class="modal-footer">
                    <a href="javascript:void(printArea('individual'))" title="Imprimir" >
                        <i class='fa fa-print fa-2x' aria-hidden='true'></i>
                    </a>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div id="myModal1" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                {!! Form::open(['action'=>'Rrhh\PersonalController@inicio', 'method'=>'post', 'id'=>'formDireccion']) !!}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Filtro de busqueda</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="seccionSeleccionada">Direccion:</label>
                        <select id="seccionSeleccionada" name="seccionSeleccionada">
                            <option value="0">TODOS</option>
                            @foreach($lsecciones as $sec)
                                <option value="{{$sec->id}}"
                                @if(strcmp($seccion,$sec->id)==0)
                                    selected="selected"
                                    @endif
                                >{{strtoupper($sec->nombre)}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="tipoSeleccionada">Tipo:</label>
                        <select id="tipoSeleccionada" name="tipoSeleccionada">
                            <option value="0">TODOS</option>
                            @foreach($ltiposemp as $tipo)
                                <option value="{{$tipo->codigo}}"
                                    @if(strcmp($tipoemp,$tipo->codigo)==0)
                                        selected="selected"
                                        @endif
                                >{{$tipo->nombre}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="tipoHorario">Horario</label>
                        <select id="tipoHorario" name="tipoHorario">
                            <option value="0">TODOS</option>
                            @foreach($lhorarios as $hor)
                                <option value="{{$hor->id}}"
                                    @if(strcmp($horario,$hor->id)==0)
                                        selected="selected"
                                        @endif
                                >{{$hor->nombre}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" onclick="selecSeccion();">
                        Buscar
                    </button>
                </div>
                @if($cesantes == 500)
                    <input type="hidden" name="cesantes" value="{{$cesantes}}" />
                @endif
                {!! Form::close() !!}
            </div>
        </div>
    </div>

@endsection
