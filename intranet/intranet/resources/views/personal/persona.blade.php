@extends('layouts.appRrhh')
@section('content')



    <div class="section">
      <div class="container">
        <div class="row">
          <div class="col-md-2">
            <img src="/img/solo_sello.png" class="img-responsive" width="100px">
          </div>
          <div class="col-md-10">
            <h1>DETALLE PERSONA</h1>
          </div>
        </div>
      </div>
    </div>
<?php
//print_r($persona);
?>

    <div class="section">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <table class="table">
              <thead>
              </thead>

              <tbody>

                <tr>
                  <th>Nombres</th>
                  <td>{{ $persona->nombre }}</td>
                </tr>
                <tr>
                  <th>Cedula</th>
                  <td>{{ $persona->cedula }}</td>
                </tr>
                <tr>
                  <th>Cuenta</th>
                  <td>{{ $persona->cuenta }}</td>
                </tr>
                <tr>
                  <th>Cargo</th>
                  <td>{{ $persona->cargos->nombre }}</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>



@endsection
