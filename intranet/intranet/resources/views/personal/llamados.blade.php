@extends('layouts.appRrhh')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                  LLAMADOS DE ATENCIÓN
                </div>
                <div class="panel-body">
                    <!--<div class="container">-->
                    <div class="form-group pull-right">
	                       <input type="text" id="buscado" class="search form-control" placeholder="Buscar?" onKeyUp="busquedaLlamados()">
                    </div>

                    <span class="counter pull-right"></span>
                    <div id="tabla">
            <table class="table">
              <thead>
                <tr>

                  <th>Nombres</th>
                  <th>Cedula</th>
                  <th>Cargo</th>
                  <th></th>
                  <th></th>
                </tr>
              </thead>
              <tbody>

                  <?php $i = 1; ?>
                  @foreach($listaPersonal as $persona)

                    <tr>
                        
                        <td>{{ $persona->nombre }}</td>
                        <td>{{ $persona->cedula }}</td>
                        <td>{{ $persona->cargos->nombre }}</td>
                        <?php
                        //$cargo  = $persona->cargos->nombre;
                        $cedula = $persona->cedula;
                        //if(strlen($cargo)>0){
                        ?>
  		                  <td>                            
                            <button type="button" class="btn btn-info btn-sm"
                                    data-toggle="modal" data-target="#myModal3"
                                    onclick="javascript:selectPersona3('{{$cedula}}','{{$persona->nombre}}')"
                                    > Uniforme </button>

                        </td>
                        <td>
                            <button type="button" class="btn btn-info btn-sm"
                                    data-toggle="modal" data-target="#myModal"
                                    onclick="javascript:selectPersona('{{$cedula}}','{{$persona->nombre}}')"
                                    > Horario </button>
                        </td> 
                        <td>
                            <button type="button" class="btn btn-info btn-sm"
                                    data-toggle="modal" data-target="#myModal1"
                                    onclick="javascript:selectPersona2('{{$cedula}}','{{$persona->nombre}}')"
                                    > Vacaciones </button>
                        </td>                        
                        <?php
                        //}
                        ?>
                    </tr>
                  @endforeach



              </tbody>
            </table>
              {!! $listaPersonal->render() !!}
            </div>


          </div>
        </div>
      </div>
    </div>
  </div>
  
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">

      {!! Form::open(array('action' => 'PdfController@llamado2', 'target'=>'blank')) !!}
      <input type="hidden" name="cedula" id="cedula"  value="{{$cedula}}" />
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Fecha del llamado de atención</h4>
        </div>
        <div class="modal-body">
          <p><b>Nombre: &nbsp;&nbsp;&nbsp;&nbsp;</b><span id="selectNombre" name="selectNombre"></span></p>
          <p><b>Fecha:</b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <input size="10" readonly type="text" id="fecha" name="fecha" value="{{$fecha}}" >                   
          </p>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-default" >Siguiente</button>
        </div> 
      </div>   
        <!--  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
      {!! Form::close() !!}  
    </div>
  </div>

  <div class="modal fade" id="myModal1" role="dialog">
    <div class="modal-dialog modal-lg">

      {!! Form::open(array('action' => 'PdfController@vacaciones', 'target'=>'blank')) !!}
      <input type="hidden" name="cedula" id="cedula2"  value="{{$cedula}}" />
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Notificación vacaciones</h4>
        </div>
        <div class="modal-body">
          <p><b>Nombre: &nbsp;&nbsp;&nbsp;&nbsp;</b><span id="selectNombre2" name="selectNombre"></span></p>
          <p><b>Fecha:</b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <input size="10" readonly type="text" id="fechav" name="fecha" value="{{$fecha}}" >                   
          </p>
          <p><b>Mes:</b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            {!! Form::select('mes',$meses, $mes) !!} 
          </p> 
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-default" >Siguiente</button>
        </div>  
       </div> 
        <!--  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
      {!! Form::close() !!}  
    </div>
  </div>

  <div class="modal fade" id="myModal3" role="dialog">
    <div class="modal-dialog modal-lg">

      {!! Form::open(array('action' => 'PdfController@llamado1', 'target'=>'blank')) !!}
      <input type="hidden" name="cedula" id="cedula3"  value="{{$cedula}}" />
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Fecha del llamado de atención</h4>
        </div>
        <div class="modal-body">
          <p><b>Nombre: &nbsp;&nbsp;&nbsp;&nbsp;</b><span id="selectNombre3" name="selectNombre"></span></p>
          <p><b>Fecha:</b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <input size="10" readonly type="text" id="fechau" name="fecha" value="{{$fecha}}" >                   
          </p>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-default" >Siguiente</button>
        </div> 
      </div>   
        <!--  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
      {!! Form::close() !!}   
    </div>
  </div>


@endsection
