@extends('layouts.appRrhh')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                  CERTIFICADOS
                </div>
                <div class="panel-body">
                    <!--<div class="container">-->
                    <div class="form-group pull-right">
	                       <input type="text" id="buscado" class="search form-control" placeholder="Buscar?" onKeyUp="busquedaCertificados()">
                    </div>

                    <span class="counter pull-right"></span>
                    <div id="tabla">
            <table class="table">
              <thead>
                <tr>

                  <th>Nombres</th>
                  <th>Cedula</th>
                  <th>Cargo</th>
                  <th></th>
                  <th></th>
                </tr>
              </thead>
              <tbody>

                  <?php $i = 1; ?>
                  @foreach($listaPersonal as $persona)

                    <tr>
                        
                        <td>{{ $persona->nombre }}</td>
                        <td>{{ $persona->cedula }}</td>
                        <td>{{ $persona->cargos->nombre }}</td>
                        <?php
                        //$cargo  = $persona->cargos->nombre;
                        $cedula = $persona->cedula;
                        //if(strlen($cargo)>0){
                        ?>
  		                  <td>
                            {!! Form::open(array('action' => array('PdfController@certificado1',$cedula), 'target'=>'blank', 'method'=>'get')) !!}
                            <button type="submit" class="btn btn-info btn-sm" > Certificado 1 </button>
                            {!! Form::close() !!}

                        </td>
                        <td>
                            <button type="button" class="btn btn-info btn-sm"
                                    data-toggle="modal" data-target="#myModal"
                                    onclick="javascript:selectPersona('{{$cedula}}','{{$persona->nombre}}')"
                                    > Certificado 2 </button>
                        </td>
                        <?php
                        //}
                        ?>
                    </tr>
                  @endforeach



              </tbody>
            </table>
              {!! $listaPersonal->render() !!}
            </div>


          </div>
        </div>
      </div>
    </div>
  </div>

    <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">

      {!! Form::open(array('action' => 'PdfController@certificado2', 'target'=>'blank')) !!}
      <input type="hidden" name="cedula" id="cedula"  value="{{ $cedula}}" />
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Tipo de relaci&oacute;n laboral</h4>
        </div>
        <div class="modal-body">
          <p><b>Nombre:</b><div id="selectNombre" name="selectNombre"></div></p>
          <p><b>Seleccione.</b></p>
          <select class="selectpicker" name="tiporelacion">
            <option>NOMBRAMIENTO PERMANENTE</option>
            <option>CONTRATO INDEFINIDO</option>
            <option>NOMBRAMIENTO DE LIBRE REMOCIÓN</option>
            <option>NOMBRAMIENTO PROVISIONAL</option>
            <option>CONTRATO SERVICIOS PROFESIONALES</option>
            <option>CONTRATO DE SERVICIOS OCASIONALES</option>
          </select>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-default" >Siguiente</button>
        <!--  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
        </div>
      </div>
      {!! Form::close() !!}





@endsection
